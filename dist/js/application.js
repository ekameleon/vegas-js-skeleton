/**
 * The VEGAS JS Skeleton application - version: 1.0.7 - license: MPL 2.0/GPL 2.0+/LGPL 2.1+ - Follow me on Twitter! @ekameleon
 */
(function (global, factory) {
	typeof exports === 'object' && typeof module !== 'undefined' ? factory(exports) :
	typeof define === 'function' && define.amd ? define(['exports'], factory) :
	(factory((global.application = {})));
}(this, (function (exports) { 'use strict';

function invoke( c , a = null )
{
    if( !(c instanceof Function) )
    {
        return null ;
    }
    if( a === null || !(a instanceof Array) || (a.length === 0)  )
    {
        return new c();
    }
    switch( a.length )
    {
        case 0:
        return new c();
        case 1:
        return new c( a[0] );
        case 2:
        return new c( a[0],a[1] );
        case 3:
        return new c( a[0],a[1],a[2] );
        case 4:
        return new c( a[0],a[1],a[2],a[3] );
        case 5:
        return new c( a[0],a[1],a[2],a[3],a[4] );
        case 6:
        return new c( a[0],a[1],a[2],a[3],a[4],a[5] );
        case 7:
        return new c( a[0],a[1],a[2],a[3],a[4],a[5],a[6] );
        case 8:
        return new c( a[0],a[1],a[2],a[3],a[4],a[5],a[6],a[7] );
        case 9:
        return new c( a[0],a[1],a[2],a[3],a[4],a[5],a[6],a[7],a[8] );
        case 10:
        return new c( a[0],a[1],a[2],a[3],a[4],a[5],a[6],a[7],a[8],a[9] );
        case 11:
        return new c( a[0],a[1],a[2],a[3],a[4],a[5],a[6],a[7],a[8],a[9],a[10] );
        case 12:
        return new c( a[0],a[1],a[2],a[3],a[4],a[5],a[6],a[7],a[8],a[9],a[10],a[11] );
        case 13:
        return new c( a[0],a[1],a[2],a[3],a[4],a[5],a[6],a[7],a[8],a[9],a[10],a[11],a[12] );
        case 14:
        return new c( a[0],a[1],a[2],a[3],a[4],a[5],a[6],a[7],a[8],a[9],a[10],a[11],a[12],a[13] );
        case 15:
        return new c( a[0],a[1],a[2],a[3],a[4],a[5],a[6],a[7],a[8],a[9],a[10],a[11],a[12],a[13],a[14] );
        case 16:
        return new c( a[0],a[1],a[2],a[3],a[4],a[5],a[6],a[7],a[8],a[9],a[10],a[11],a[12],a[13],a[14],a[15] );
        case 17:
        return new c( a[0],a[1],a[2],a[3],a[4],a[5],a[6],a[7],a[8],a[9],a[10],a[11],a[12],a[13],a[14],a[15],a[16] );
        case 18:
        return new c( a[0],a[1],a[2],a[3],a[4],a[5],a[6],a[7],a[8],a[9],a[10],a[11],a[12],a[13],a[14],a[15],a[16],
                      a[17] );
        case 19:
        return new c( a[0],a[1],a[2],a[3],a[4],a[5],a[6],a[7],a[8],a[9],a[10],a[11],a[12],a[13],a[14],a[15],a[16],
                      a[17],a[18] );
        case 20:
        return new c( a[0],a[1],a[2],a[3],a[4],a[5],a[6],a[7],a[8],a[9],a[10],a[11],a[12],a[13],a[14],a[15],a[16],
                      a[17],a[18],a[19] );
        case 21:
        return new c( a[0],a[1],a[2],a[3],a[4],a[5],a[6],a[7],a[8],a[9],a[10],a[11],a[12],a[13],a[14],a[15],a[16],
                      a[17],a[18],a[19],a[20] );
        case 22:
        return new c( a[0],a[1],a[2],a[3],a[4],a[5],a[6],a[7],a[8],a[9],a[10],a[11],a[12],a[13],a[14],a[15],a[16],
                      a[17],a[18],a[19],a[20],a[21] );
        case 23:
        return new c( a[0],a[1],a[2],a[3],a[4],a[5],a[6],a[7],a[8],a[9],a[10],a[11],a[12],a[13],a[14],a[15],a[16],
                      a[17],a[18],a[19],a[20],a[21],a[22] );
        case 24:
        return new c( a[0],a[1],a[2],a[3],a[4],a[5],a[6],a[7],a[8],a[9],a[10],a[11],a[12],a[13],a[14],a[15],a[16],
                      a[17],a[18],a[19],a[20],a[21],a[22],a[23] );
        case 25:
        return new c( a[0],a[1],a[2],a[3],a[4],a[5],a[6],a[7],a[8],a[9],a[10],a[11],a[12],a[13],a[14],a[15],a[16],
                      a[17],a[18],a[19],a[20],a[21],a[22],a[23],a[24] );
        case 26:
        return new c( a[0],a[1],a[2],a[3],a[4],a[5],a[6],a[7],a[8],a[9],a[10],a[11],a[12],a[13],a[14],a[15],a[16],
                      a[17],a[18],a[19],a[20],a[21],a[22],a[23],a[24],a[25] );
        case 27:
        return new c( a[0],a[1],a[2],a[3],a[4],a[5],a[6],a[7],a[8],a[9],a[10],a[11],a[12],a[13],a[14],a[15],a[16],
                      a[17],a[18],a[19],a[20],a[21],a[22],a[23],a[24],a[25],a[26] );
        case 28:
        return new c( a[0],a[1],a[2],a[3],a[4],a[5],a[6],a[7],a[8],a[9],a[10],a[11],a[12],a[13],a[14],a[15],a[16],
                      a[17],a[18],a[19],a[20],a[21],a[22],a[23],a[24],a[25],a[26],a[27] );
        case 29:
        return new c( a[0],a[1],a[2],a[3],a[4],a[5],a[6],a[7],a[8],a[9],a[10],a[11],a[12],a[13],a[14],a[15],a[16],
                      a[17],a[18],a[19],a[20],a[21],a[22],a[23],a[24],a[25],a[26],a[27],a[28] );
        case 30:
        return new c( a[0],a[1],a[2],a[3],a[4],a[5],a[6],a[7],a[8],a[9],a[10],a[11],a[12],a[13],a[14],a[15],a[16],
                      a[17],a[18],a[19],a[20],a[21],a[22],a[23],a[24],a[25],a[26],a[27],a[28],a[29] );
        case 31:
        return new c( a[0],a[1],a[2],a[3],a[4],a[5],a[6],a[7],a[8],a[9],a[10],a[11],a[12],a[13],a[14],a[15],a[16],
                      a[17],a[18],a[19],a[20],a[21],a[22],a[23],a[24],a[25],a[26],a[27],a[28],a[29],a[30] );
        case 32:
        return new c( a[0],a[1],a[2],a[3],a[4],a[5],a[6],a[7],a[8],a[9],a[10],a[11],a[12],a[13],a[14],a[15],a[16],
                      a[17],a[18],a[19],a[20],a[21],a[22],a[23],a[24],a[25],a[26],a[27],a[28],a[29],a[30],a[31] );
        default:
        return null;
    }
}

function Evaluable() {}
Evaluable.prototype = Object.create( Object.prototype ,
{
    constructor : { writable : true , value : Evaluable } ,
    eval : { writable : true , value : function( value ) {} } ,
    toString : { writable : true , value : function() { return '[' + this.constructor.name + ']' ; } }
});

function KeyValuePair() {}
KeyValuePair.prototype = Object.create( Object.prototype ,
{
    constructor : { writable : true , value :  KeyValuePair },
    length : { get : () => 0 } ,
    clear : { value : function () {} , writable : true } ,
    clone : { value : function () { return new KeyValuePair() } , writable : true } ,
    copyFrom : { value : function( map ) {} , writable : true } ,
    delete : { value : function( key ) {} , writable : true } ,
    forEach : { value : function( callback , thisArg = null ) {} , writable : true } ,
    get : { value : function( key ) { return null ; } , writable : true } ,
    has : { value : function( key ) { return false ; } , writable : true } ,
    hasValue : { value : function( value ) { return false ; } , writable : true } ,
    isEmpty : { value : function() { return false ; } , writable : true } ,
    iterator : { value : function()              { return null ; } , writable : true } ,
    keyIterator : { value : function()              { return null } , writable : true } ,
    keys : { value : function() { return null ; } , writable : true } ,
    set : { value : function( key , value ) {} , writable : true } ,
    toString : { value : function () { return '[' + this.constructor.name + ']' ; } , writable : true } ,
    values : { value : function ()  {} , writable : true }
}) ;

function MapEntry( key , value )
{
    Object.defineProperties( this ,
    {
        key : { value : key , writable : true },
        value : { value : value , writable : true }
    }) ;
}
MapEntry.prototype = Object.create( Object.prototype ,
{
    constructor : { value : MapEntry } ,
    clone : { value : function()
    {
        return new MapEntry( this.key , this.value ) ;
    }},
    toString : { value : function()
    {
        return "[MapEntry key:" + this.key + " value:" + this.value + "]" ;
    }}
}) ;

function MapFormatter() {}
MapFormatter.prototype = Object.create( Object.prototype ,
{
    constructor : { writable : true , value : MapFormatter },
    format : { value : function( value )
    {
        if ( value instanceof KeyValuePair )
        {
            let r = "{";
            let keys = value.keys();
            let len  = keys.length;
            if( len > 0 )
            {
                let values = value.values();
                for( let i = 0 ; i < len ; i++ )
                {
                    r += keys[i] + ':' + values[i] ;
                    if( i < len - 1 )
                    {
                        r += "," ;
                    }
                }
            }
            r += "}" ;
            return r ;
        }
        else
        {
            return "{}" ;
        }
    }}
}) ;
var formatter = new MapFormatter();

function Iterator() {}
Iterator.prototype = Object.create( Object.prototype ,
{
    constructor : { writable : true , value : Iterator } ,
    delete : { writable : true , value : function() {} } ,
    hasNext : { writable : true , value : function() {} } ,
    key : { writable : true , value : function() {} } ,
    next : { writable : true , value : function() {} } ,
    reset : { writable : true , value : function() {} } ,
    seek : { writable : true , value : function ( position ) {} } ,
    toString :
    {
        writable : true , value : function ()
        {
            if( !('__clazzname__' in this.constructor) )
            {
                Object.defineProperty( this.constructor , '__clazzname__' , { value : this.constructor.name } ) ;
            }
            return '[' + this.constructor.__clazzname__ + ']' ;
        }
    }
}) ;

function ArrayIterator( array )
{
    if ( !(array instanceof Array) )
    {
        throw new ReferenceError( this + " constructor failed, the passed-in Array argument not must be 'null'.") ;
    }
    Object.defineProperties( this ,
    {
        _a : { value : array , writable : true } ,
        _k : { value : -1    , writable : true }
    }) ;
}
ArrayIterator.prototype = Object.create( Iterator.prototype ,
{
    constructor : { value : ArrayIterator } ,
    delete : { value : function()
    {
        return this._a.splice(this._k--, 1)[0] ;
    }},
    hasNext : { value : function()
    {
        return (this._k < this._a.length -1) ;
    }},
    key : { value : function()
    {
        return this._k ;
    }},
    next : { value : function()
    {
        return this._a[++this._k] ;
    }},
    reset : { value : function()
    {
        this._k = -1 ;
    }},
    seek : { value : function ( position )
    {
        position = Math.max( Math.min( position - 1 , this._a.length ) , -1 ) ;
        this._k = isNaN(position) ? -1 : position ;
    }}
}) ;

function MapIterator( map )
{
    if ( map && ( map instanceof KeyValuePair) )
    {
        Object.defineProperties( this ,
        {
            _m : { value : map  , writable : true } ,
            _i : { value : new ArrayIterator( map.keys() ) , writable : true } ,
            _k : { value : null , writable : true }
        }) ;
    }
    else
    {
       throw new ReferenceError( this + " constructor failed, the passed-in KeyValuePair argument not must be 'null'.") ;
    }
}
MapIterator.prototype = Object.create( Iterator.prototype ,
{
    constructor : { writable : true , value : MapIterator } ,
    delete : { value : function()
    {
        this._i.delete() ;
        return this._m.delete( this._k ) ;
    }},
    hasNext : { value : function()
    {
        return this._i.hasNext() ;
    }},
    key : { value : function()
    {
        return this._k ;
    }},
    next : { value : function()
    {
        this._k = this._i.next() ;
        return this._m.get( this._k ) ;
    }},
    reset : { value : function()
    {
        this._i.reset() ;
    }},
    seek : { value : function ( position )
    {
        throw new Error( "This Iterator does not support the seek() method.") ;
    }}
}) ;

function ArrayMap( keys = null , values = null )
{
    Object.defineProperties( this ,
    {
        _keys :
        {
            value : [] ,
            writable : true
        },
        _values :
        {
            value : [] ,
            writable : true
        }
    }) ;
    if ( keys === null || values === null )
    {
        this._keys   = [] ;
        this._values = [] ;
    }
    else
    {
        let b = ( keys instanceof Array && values instanceof Array && keys.length > 0 && keys.length === values.length );
        this._keys   = b ? [].concat(keys)   : [] ;
        this._values = b ? [].concat(values) : [] ;
    }
}
ArrayMap.prototype = Object.create( KeyValuePair.prototype ,
{
    constructor : { writable : true , value : ArrayMap } ,
    length : { get : function() { return this._keys.length ; } } ,
    clear : { value : function ()
    {
        this._keys   = [] ;
        this._values = [] ;
    }},
    clone : { value : function ()
    {
        return new ArrayMap( this._keys , this._values ) ;
    }},
    copyFrom : { value : function ( map )
    {
        if ( !map || !(map instanceof KeyValuePair) )
        {
            return ;
        }
        let keys = map.keys();
        let values = map.values();
        let l = keys.length;
        for ( let i = 0 ; i<l ; i = i - (-1) )
        {
            this.set(keys[i], values[i]) ;
        }
    }},
    delete : { value : function ( key )
    {
        let v = null;
        let i = this.indexOfKey( key );
        if ( i > -1 )
        {
            v = this._values[i] ;
            this._keys.splice(i, 1) ;
            this._values.splice(i, 1) ;
        }
        return v ;
    }},
    forEach : { value : function( callback , thisArg = null )
    {
        if (typeof callback !== "function")
        {
            throw new TypeError( callback + ' is not a function' );
        }
        let l = this._keys.length;
        for( let i = 0 ; i<l ; i++ )
        {
            callback.call( thisArg , this._values[i] , this._keys[i] , this ) ;
        }
    }},
    get : { value : function( key )
    {
        return this._values[ this.indexOfKey( key ) ] ;
    }},
    getKeyAt : { value : function ( index )
    {
        return this._keys[ index ] ;
    }},
    getValueAt : { value : function ( index          )
    {
        return this._values[ index ] ;
    }},
    has : { value : function ( key )
    {
        return this.indexOfKey(key) > -1 ;
    }},
    hasValue : { value : function ( value )
    {
        return this.indexOfValue( value ) > -1 ;
    }},
    indexOfKey : { value : function (key)
    {
        let l = this._keys.length;
        while( --l > -1 )
        {
            if ( this._keys[l] === key )
            {
                return l ;
            }
        }
        return -1 ;
    }},
    indexOfValue : { value : function (value)
    {
        let l = this._values.length;
        while( --l > -1 )
        {
            if ( this._values[l] === value )
            {
                return l ;
            }
        }
        return -1 ;
    }},
    isEmpty : { value : function ()
    {
        return this._keys.length === 0 ;
    }},
    iterator : { value : function ()
    {
        return new MapIterator( this ) ;
    }},
    keyIterator : { value : function ()
    {
        return new ArrayIterator( this._keys ) ;
    }},
    keys : { value : function ()
    {
        return this._keys.concat() ;
    }},
    set : { value : function ( key , value )
    {
        let r = null;
        let i = this.indexOfKey( key );
        if ( i < 0 )
        {
            this._keys.push( key ) ;
            this._values.push( value ) ;
        }
        else
        {
            r = this._values[i] ;
            this._values[i] = value ;
        }
        return r ;
    }},
    setKeyAt : { value : function( index , key )
    {
        if ( index >= this._keys.length )
        {
            throw new RangeError( "ArrayMap.setKeyAt(" + index + ") failed with an index out of the range.") ;
        }
        if ( this.containsKey( key ) )
        {
            return null ;
        }
        let k = this._keys[index];
        if (k === undefined)
        {
            return null ;
        }
        let v = this._values[index];
        this._keys[index] = key ;
        return new MapEntry( k , v ) ;
    }},
    setValueAt : { value : function( index , value )
    {
        if ( index >= this._keys.length )
        {
            throw new RangeError( "ArrayMap.setValueAt(" + index + ") failed with an index out of the range.") ;
        }
        let v = this._values[index];
        if (v === undefined)
        {
            return null ;
        }
        let k = this._keys[index];
        this._values[index] = value ;
        return new MapEntry( k , v ) ;
    }},
    toString : { value : function () { return formatter.format( this ) ; }},
    values : { value : function ()
    {
        return this._values.concat() ;
    }}
}) ;

function isIdentifiable( target )
{
    if( target )
    {
        return (target instanceof Identifiable) || ('id' in target) ;
    }
    return false ;
}
function Identifiable()
{
    Object.defineProperties( this ,
    {
        id : { value : null , writable : true }
    }) ;
}
Identifiable.prototype = Object.create( Object.prototype ,
{
    constructor : { writable : true , value :  Identifiable }
}) ;

function MultiEvaluator( elements = null )
{
    Object.defineProperties( this ,
    {
        autoClear : { value : false , writable : true } ,
        _evaluators : { value : [] , writable : true }
    }) ;
    if ( elements instanceof Array && elements.length > 0 )
    {
        this.add.apply( this, elements ) ;
    }
}
MultiEvaluator.prototype = Object.create( Evaluable.prototype ,
{
    length :
    {
        get : function()
        {
            return this._evaluators.length ;
        }
    },
    add : { value : function ( ...evaluators )
    {
        if ( this.autoClear )
        {
            this.clear() ;
        }
        var l = evaluators.length;
        if ( l > 0 )
        {
            var c, i, j;
            var e;
            for ( i = 0 ; i < l ; i++ )
            {
                e = evaluators[i] ;
                if ( e instanceof Evaluable )
                {
                    this._evaluators.push( e ) ;
                }
                else if ( e instanceof Array )
                {
                    c = e.length ;
                    for ( j = 0 ; j < c ; j++ )
                    {
                        if ( e[j] instanceof Evaluable )
                        {
                            this._evaluators.push( e[j] ) ;
                        }
                    }
                }
            }
        }
    }},
    clear : { value : function()
    {
        this._evaluators = [] ;
    }},
    eval : { value : function( value )
    {
        this._evaluators.forEach( ( element ) =>
        {
            if( element instanceof Evaluable )
            {
                value = element.eval( value ) ;
            }
        }) ;
        return value ;
    }},
    remove : { value : function( evaluator )
    {
        if( evaluator instanceof Evaluable )
        {
            var index = this._evaluators.indexOf( evaluator );
            if( index > - 1 )
            {
                this._evaluators.splice( index , 1 ) ;
                return true ;
            }
        }
        return false ;
    }}
});
MultiEvaluator.prototype.constructor = MultiEvaluator ;
MultiEvaluator.prototype.toString = function ()
{
    return "[MultiEvaluator]" ;
};

function EventListener() {}
EventListener.prototype = Object.create( Object.prototype ,
{
    constructor : { writable : true , value : EventListener } ,
    handleEvent : { writable : true , value : function() {} } ,
    toString : { writable : true , value : function ()
    {
        return '[' + this.constructor.name + ']' ;
    }}
});

function IEventDispatcher() {}
IEventDispatcher.prototype = Object.create( Object.prototype ,
{
    constructor : { writable : true , value : IEventDispatcher },
    addEventListener : { writable : true , value : function( type, listener, useCapture = false, priority = 0 ) {} },
    dispatchEvent : { writable : true , value : function( event ) {} },
    hasEventListener : { writable : true , value : function( type ) {} },
    removeEventListener : { writable : true , value : function( type, listener, useCapture = false ) {} },
    willTrigger : { writable : true , value : function( type ) {} }
});

function Receiver() {}
Receiver.prototype = Object.create( Object.prototype ,
{
    constructor : { writable : true , value : Receiver } ,
    receive : { writable : true , value : function() {} } ,
    toString : { writable : true , value : function ()
    {
        return '[' + this.constructor.name + ']' ;
    }}
});

function Signaler() {}
Signaler.prototype = Object.create( Object.prototype,
{
    constructor : { writable : true , value : Signaler } ,
    length : { get : function () { return 0 ; }} ,
    connect : { writable : true , value : function( receiver , priority = 0 , autoDisconnect = false ) {} },
    connected : { writable : true , value : function() {} },
    disconnect: { writable : true , value : function( receiver ) {} },
    emit: { writable : true , value : function() {} },
    hasReceiver : { writable : true , value : function( receiver ) {} }
});

function isLockable( target )
{
    if( target )
    {
        if( target instanceof Lockable )
        {
            return true ;
        }
        else
        {
            return Boolean( target['isLocked'] ) && ( target.isLocked instanceof Function ) &&
                   Boolean( target['lock'] )     && ( target.lock     instanceof Function ) &&
                   Boolean( target['unlock'] )   && ( target.unlock   instanceof Function ) ;
        }
    }
    return false ;
}
function Lockable()
{
    Object.defineProperties( this ,
    {
        __lock__ : { writable : true  , value : false }
    }) ;
}
Lockable.prototype = Object.create( Object.prototype ,
{
    constructor : { writable : true , value : Lockable } ,
    isLocked : { writable : true , value : function()
    {
        return this.__lock__ ;
    }},
    lock : { writable : true , value : function()
    {
        this.__lock__ = true ;
    }},
    unlock : { writable : true , value : function()
    {
        this.__lock__ = false ;
    }},
    toString : { writable : true , value : function()
    {
        return '[' + this.constructor.name + ']' ;
    }}
});

var ObjectAttribute = Object.defineProperties( {} ,
{
    ARGUMENTS : { value : 'args' , enumerable : true },
    CALLBACK : { value : 'callback' , enumerable : true },
    CONFIG : { value : 'config' , enumerable : true },
    DEPENDS_ON : { value : 'dependsOn' , enumerable : true },
    DESTROY_METHOD_NAME : { value : 'destroy' , enumerable : true },
    EVALUATORS : { value : 'evaluators' , enumerable : true },
    FACTORY : { value : 'factory' , enumerable : true },
    GENERATES : { value : 'generates' , enumerable : true },
    ID : { value : 'id' , enumerable : true },
    IDENTIFY : { value : 'identify' , enumerable : true },
    INIT_METHOD_NAME : { value : 'init' , enumerable : true },
    LAZY_INIT : { value : 'lazyInit' , enumerable : true },
    LAZY_TYPE : { value : 'lazyType' , enumerable : true },
    LISTENERS : { value : 'listeners' , enumerable : true },
    LOCALE : { value : 'locale' , enumerable : true },
    LOCK : { value : 'lock' , enumerable : true },
    NAME : { value : 'name' , enumerable : true },
    PROPERTIES : { value : 'properties' , enumerable : true },
    RECEIVERS : { value : 'receivers' , enumerable : true },
    REFERENCE : { value : 'ref' , enumerable : true },
    SCOPE : { value : 'scope' , enumerable : true },
    SINGLETON : { value : 'singleton' , enumerable : true },
    TYPE : { value : 'type' , enumerable : true },
    VALUE : { value : 'value' , enumerable : true }
});

function ObjectArgument( value , policy = "value" , evaluators = null )
{
    Object.defineProperties( this ,
    {
        args : { value : null, writable : true } ,
        evaluators : { value : evaluators instanceof Array ? evaluators : null, writable : true } ,
        scope : { value : null, writable : true } ,
        value : { value : value , writable : true } ,
        _policy : { value : null , writable : true }
    });
    this.policy = policy ;
}
ObjectArgument.prototype = Object.create( Object.prototype ,
{
    constructor : { value : ObjectArgument } ,
    policy :
    {
        get : function policy()
        {
            return this._policy ;
        },
        set : function( str )
        {
            switch (str)
            {
                case ObjectAttribute.CALLBACK  :
                case ObjectAttribute.REFERENCE :
                case ObjectAttribute.CONFIG    :
                case ObjectAttribute.LOCALE    :
                {
                    this._policy = str ;
                    break ;
                }
                default :
                {
                    this._policy = ObjectAttribute.VALUE ;
                }
            }
        }
    },
    toString : { value : function () { return '[ObjectArgument]' ; }}
});

function createArguments( a )
{
    if ( !(a instanceof Array) || a.length === 0 )
    {
        return null ;
    }
    else
    {
        let args = [];
        let l = a.length;
        for ( let i = 0 ; i<l ; i++ )
        {
            let o = a[i];
            if ( o !== null )
            {
                let call       = ( ObjectAttribute.CALLBACK in o )   ? o[ ObjectAttribute.CALLBACK ]  : null;
                let conf       = ( ObjectAttribute.CONFIG in o )     ? String(o[ ObjectAttribute.CONFIG ]) : null;
                let i18n       = ( ObjectAttribute.LOCALE in o )     ? String(o[ ObjectAttribute.LOCALE ]) : null;
                let ref        = ( ObjectAttribute.REFERENCE in o )  ? String(o[ ObjectAttribute.REFERENCE ]) : null;
                let value      = ( ObjectAttribute.VALUE in o )      ? o[ ObjectAttribute.VALUE ] : null;
                let evaluators = ( ObjectAttribute.EVALUATORS in o ) ? o[ ObjectAttribute.EVALUATORS ] : null;
                if ( ref !== null && ref.length > 0 )
                {
                    args.push( new ObjectArgument( ref , ObjectAttribute.REFERENCE , evaluators ) ) ;
                }
                else if ( conf !== null && conf.length > 0 )
                {
                    args.push( new ObjectArgument( conf , ObjectAttribute.CONFIG , evaluators ) ) ;
                }
                else if ( i18n !== null && i18n.length > 0 )
                {
                    args.push( new ObjectArgument( i18n , ObjectAttribute.LOCALE , evaluators ) ) ;
                }
                else if ( call instanceof Function || ( (call instanceof String || typeof(call) === 'string') && (call !== '') ) )
                {
                    let def = new ObjectArgument( call , ObjectAttribute.CALLBACK , evaluators );
                    if( ObjectAttribute.SCOPE in o )
                    {
                        def.scope = o[ObjectAttribute.SCOPE] ;
                    }
                    if( ( ObjectAttribute.ARGUMENTS in o ) && (o[ ObjectAttribute.ARGUMENTS ] instanceof Array) )
                    {
                        def.args = createArguments( o[ ObjectAttribute.ARGUMENTS ] ) ;
                    }
                    args.push( def ) ;
                }
                else
                {
                    args.push( new ObjectArgument( value , ObjectAttribute.VALUE , evaluators ) ) ;
                }
            }
        }
        return args.length > 0 ? args : null ;
    }
}

function dumpArray( value  , prettyprint = false , indent = 0 , indentor = "    " )
{
    indent = isNaN(indent) ? 0 : indent ;
    prettyprint = Boolean( prettyprint ) ;
    if( !indentor )
    {
        indentor = "    " ;
    }
    var source  = [];
    var i;
    var l  = value.length;
    for( i = 0 ; i < l ; i++ )
    {
        if( value[i] === undefined )
        {
            source.push( "undefined" );
            continue;
        }
        if( value[i] === null )
        {
            source.push( "null" );
            continue;
        }
        if( prettyprint )
        {
            indent++ ;
        }
        source.push( dump( value[i], prettyprint, indent, indentor ) ) ;
        if( prettyprint )
        {
            indent-- ;
        }
    }
    if( prettyprint )
    {
        var spaces  = [];
        for( i=0 ; i < indent ; i++ )
        {
            spaces.push( indentor );
        }
        var decal  = "\n" + spaces.join( "" );
        return decal + "[" + decal + indentor + source.join( "," + decal + indentor ) + decal + "]" ;
    }
    else
    {
        return "[" + source.join( "," ) + "]" ;
    }
}

function dumpDate( date          , timestamp = false  )
{
    timestamp = Boolean( timestamp ) ;
    if ( timestamp )
    {
        return "new Date(" + String( date.valueOf() ) + ")";
    }
    else
    {
        var y    = date.getFullYear();
        var m    = date.getMonth();
        var d    = date.getDate();
        var h    = date.getHours();
        var mn   = date.getMinutes();
        var s    = date.getSeconds();
        var ms   = date.getMilliseconds();
        var data = [ y, m, d, h, mn, s, ms ];
        data.reverse();
        while( data[0] === 0 )
        {
            data.splice( 0, 1 );
        }
        data.reverse() ;
        return "new Date(" + data.join( "," ) + ")";
    }
}

function dumpObject( value , prettyprint = false , indent = 0 , indentor = "    ")
{
    indent = isNaN(indent) ? 0 : indent ;
    prettyprint = Boolean( prettyprint ) ;
    if( !indentor )
    {
        indentor = "    " ;
    }
    var source  = [];
    for( var member  in value )
    {
        if( value.hasOwnProperty( member ) )
        {
            if( value[member] === undefined )
            {
                source.push( member + ":" + "undefined" );
                continue;
            }
            if( value[member] === null )
            {
                source.push( member + ":" + "null" );
                continue;
            }
            if( prettyprint )
            {
                indent++ ;
            }
            source.push( member + ":" + dump( value[ member ], prettyprint, indent, indentor ) );
            if( prettyprint )
            {
                indent-- ;
            }
        }
    }
    source = source.sort();
    if( prettyprint )
    {
        let spaces = [];
        for( var i = 0 ; i < indent ; i++ )
        {
            spaces.push( indentor );
        }
        let decal = '\n' + spaces.join( '' );
        return decal + '{' + decal + indentor + source.join( ',' + decal + indentor ) + decal + '}' ;
    }
    else
    {
        return( '{' + source.join( ',' ) + '}' ) ;
    }
}

function toUnicodeNotation( num )
{
    var hex = num.toString( 16 );
    while( hex.length < 4 )
    {
        hex = "0" + hex ;
    }
    return hex ;
}

function dumpString( value   )
{
    var code;
    var quote  = "\"";
    var str    = "";
    var ch     = "";
    var pos       = 0;
    var len       = value.length;
    while( pos < len )
    {
        ch  = value.charAt( pos );
        code = value.charCodeAt( pos );
        if( code > 0xFF )
        {
            str += "\\u" + toUnicodeNotation( code );
            pos++;
            continue;
        }
        switch( ch )
        {
            case "\u0008" :
            {
                str += "\\b" ;
                break;
            }
            case "\u0009" :
            {
                str += "\\t" ;
                break;
            }
            case "\u000A" :
            {
                str += "\\n" ;
                break;
            }
            case "\u000B" :
            {
                str += "\\v" ;
                break;
            }
            case "\u000C" :
            {
                str += "\\f" ;
                break;
            }
            case "\u000D" :
            {
                str += "\\r" ;
                break;
            }
            case "\u0022" :
            {
                str += "\\\"" ;
                break;
            }
            case "\u0027" :
            {
                str += "\\\'";
                break;
            }
            case "\u005c" :
            {
                str += "\\\\";
                break;
            }
            default :
            {
                str += ch;
            }
        }
        pos++;
    }
    return quote + str + quote;
}

function dump( o , prettyprint  , indent  , indentor   )
{
    indent = isNaN(indent) ? 0 : indent ;
    prettyprint = Boolean( prettyprint ) ;
    if( !indentor )
    {
        indentor = "    " ;
    }
    if( o === undefined )
    {
        return "undefined";
    }
    else if( o === null )
    {
        return "null";
    }
    else if( typeof(o) === "string" || o instanceof String )
    {
        return dumpString( o );
    }
    else if ( typeof(o) === "boolean" || o instanceof Boolean  )
    {
        return o ? "true" : "false";
    }
    else if( typeof(o) === "number" || o instanceof Number )
    {
        return o.toString() ;
    }
    else if( o instanceof Date )
    {
        return dumpDate( o );
    }
    else if( o instanceof Array )
    {
        return dumpArray( o , prettyprint, indent, indentor );
    }
    else if( o.constructor && o.constructor === Object )
    {
        return dumpObject( o , prettyprint, indent, indentor );
    }
    else if( "toSource" in o )
    {
        return o.toSource( indent );
    }
    else
    {
        return "<unknown>";
    }
}

function indexOfAny( source , anyOf , startIndex = 0 , count = -1 )
{
    if( !(source instanceof String || typeof(source) === 'string' ) || source === "" )
    {
        return -1 ;
    }
    if( !(anyOf instanceof Array) )
    {
        return -1 ;
    }
    startIndex = startIndex > 0 ? 0 : startIndex ;
    count      = count < 0 ? -1 : count ;
    let l = source.length;
    let endIndex;
    if( ( count < 0 ) || ( count > l - startIndex ) )
    {
        endIndex = l - 1 ;
    }
    else
    {
        endIndex = startIndex + count - 1;
    }
    for( let i = startIndex ; i <= endIndex ; i++ )
    {
        if( anyOf.indexOf( source[i] ) > - 1 )
        {
            return i ;
        }
    }
    return -1 ;
}

function InvalidChannelError( message , fileName , lineNumber )
{
    this.name = 'InvalidChannelError';
    this.message    = message || 'invalid channel error' ;
    this.fileName   = fileName ;
    this.lineNumber = lineNumber ;
    this.stack      = (new Error()).stack;
}
InvalidChannelError.prototype = Object.create( Error.prototype );
InvalidChannelError.prototype.constructor = InvalidChannelError;

function SignalEntry( receiver , priority = 0 , auto = false )
{
    this.auto = auto ;
    this.receiver = receiver ;
    this.priority = priority ;
}
SignalEntry.prototype = Object.create( Object.prototype );
SignalEntry.prototype.constructor = SignalEntry;
SignalEntry.prototype.toString = function()
{
    return '[SignalEntry]' ;
};

function Signal()
{
    Object.defineProperties( this ,
    {
        proxy : { value : null, configurable : true , writable : true } ,
        receivers : { writable : true , value : [] }
    }) ;
}
Signal.prototype = Object.create( Signaler.prototype ,
{
    constructor : { value : Signal , writable : true },
    length : { get : function() { return this.receivers.length ; } },
    connect : { value : function ( receiver , priority = 0 , autoDisconnect = false )
    {
        if ( receiver === null )
        {
            return false ;
        }
        autoDisconnect = autoDisconnect === true ;
        priority = priority > 0 ? (priority - (priority % 1)) : 0 ;
        if ( ( typeof(receiver) === "function" ) || ( receiver instanceof Function ) || ( receiver instanceof Receiver ) || ( "receive" in receiver ) )
        {
            if ( this.hasReceiver( receiver ) )
            {
                return false ;
            }
            this.receivers.push( new SignalEntry( receiver , priority , autoDisconnect ) ) ;
            let i;
            let j;
            let a = this.receivers;
            let swap = function( j , k )
            {
                var temp = a[j];
                a[j]     = a[k] ;
                a[k]     = temp ;
                return true ;
            };
            let swapped = false;
            let l = a.length;
            for( i = 1 ; i < l ; i++ )
            {
                for( j = 0 ; j < ( l - i ) ; j++ )
                {
                    if ( a[j+1].priority > a[j].priority )
                    {
                        swapped = swap(j, j+1) ;
                    }
                }
                if ( !swapped )
                {
                    break;
                }
            }
            return true ;
        }
        return false ;
    }},
    connected : { value : function ()
    {
        return this.receivers.length > 0 ;
    }},
    disconnect : { value : function ( receiver = null )
    {
        if ( receiver === null )
        {
            if ( this.receivers.length > 0 )
            {
                this.receivers = [] ;
                return true ;
            }
            else
            {
                return false ;
            }
        }
        if ( this.receivers.length > 0 )
        {
            var l  = this.receivers.length;
            while( --l > -1 )
            {
                if ( this.receivers[l].receiver === receiver )
                {
                    this.receivers.splice( l , 1 ) ;
                    return true ;
                }
            }
        }
        return false ;
    }},
    emit : { value : function( ...values )
    {
        let l = this.receivers.length;
        if ( l === 0 )
        {
            return ;
        }
        let i;
        let r = [];
        let a = this.receivers.slice();
        let e;
        var slot;
        for ( i = 0 ; i < l ; i++ )
        {
            e = a[i] ;
            if ( e.auto )
            {
                r.push( e )  ;
            }
        }
        if ( r.length > 0 )
        {
            l = r.length ;
            while( --l > -1 )
            {
                i = this.receivers.indexOf( r[l] ) ;
                if ( i > -1 )
                {
                    this.receivers.splice( i , 1 ) ;
                }
            }
        }
        l = a.length ;
        for ( i = 0 ; i<l ; i++ )
        {
            slot = a[i].receiver ;
            if( slot instanceof Function || typeof(receiver) === "function" )
            {
                slot.apply( this.proxy || this , values ) ;
            }
            else if ( slot instanceof Receiver || ( "receive" in slot && (slot.receive instanceof Function) ) )
            {
                slot.receive.apply( this.proxy || slot , values ) ;
            }
        }
    }},
    hasReceiver : { value : function ( receiver )
    {
        if ( receiver === null )
        {
            return false ;
        }
        if ( this.receivers.length > 0 )
        {
            let l = this.receivers.length;
            while( --l > -1 )
            {
                if ( this.receivers[l].receiver === receiver )
                {
                    return true ;
                }
            }
        }
        return false ;
    }},
    toArray : { value : function()
    {
        let r = [];
        let l = this.receivers.length;
        if ( l > 0 )
        {
            for( let i=0 ; i<l ; i++ )
            {
                r.push( this.receivers[i].receiver ) ;
            }
        }
        return r ;
    }},
    toString : { value : function () { return '[Signal]' ; }}
});

function Enum( value , name )
{
    Object.defineProperties( this ,
    {
        _name :
        {
            value        : ((typeof(name) === "string" || name instanceof String )) ? name : "" ,
            enumerable   : false ,
            writable     : true ,
            configurable : true
        },
        _value :
        {
            value        : isNaN(value) ? 0 : value ,
            enumerable   : false ,
            writable     : true ,
            configurable : true
        }
    }) ;
}
Enum.prototype = Object.create( Object.prototype );
Enum.prototype.constructor = Enum;
Enum.prototype.equals = function ( object )
{
    if ( object === this )
    {
        return true ;
    }
    if( object instanceof Enum )
    {
        return (object.toString() === this.toString()) && (object.valueOf() === this.valueOf()) ;
    }
    return false ;
};
Enum.prototype.toString = function()
{
    return this._name ;
};
Enum.prototype.valueOf = function()
{
    return this._value ;
};

function LoggerLevel( value , name )
{
    Enum.call( this , value , name ) ;
}
LoggerLevel.prototype = Object.create( Enum.prototype );
LoggerLevel.prototype.constructor = LoggerLevel;
Object.defineProperties( LoggerLevel ,
{
    ALL : { value : new LoggerLevel( 1 , 'ALL' ) , enumerable : true } ,
    CRITICAL : { value : new LoggerLevel( 16 , 'CRITICAL' ) , enumerable : true } ,
    DEBUG : { value : new LoggerLevel( 2 , 'DEBUG' ) , enumerable : true } ,
    DEFAULT_LEVEL_STRING : { value : 'UNKNOWN' , enumerable : true } ,
    ERROR : { value : new LoggerLevel( 8 , 'ERROR' ) , enumerable : true } ,
    INFO : { value : new LoggerLevel( 4 , 'INFO' ) , enumerable : true } ,
    NONE : { value : new LoggerLevel( 0 , 'NONE' ) , enumerable : true } ,
    WARNING : { value : new LoggerLevel( 6 , 'WARNING' ) , enumerable : true } ,
    WTF : { value : new LoggerLevel( 32 , 'WTF' ) , enumerable : true } ,
    get : { value : function( value )
    {
        let levels =
        [
            LoggerLevel.ALL,
            LoggerLevel.CRITICAL,
            LoggerLevel.DEBUG,
            LoggerLevel.ERROR,
            LoggerLevel.INFO,
            LoggerLevel.NONE,
            LoggerLevel.WARNING,
            LoggerLevel.WTF
        ];
        let l = levels.length;
        while( --l > -1 )
        {
            if ( levels[l]._value === value )
            {
                return levels[l] ;
            }
        }
        return null ;
    }},
    getLevelString : { value : function( value )
    {
        if ( LoggerLevel.validate( value ) )
        {
            return value.toString() ;
        }
        else
        {
            return LoggerLevel.DEFAULT_LEVEL_STRING ;
        }
    }},
    validate : { value : function( level )
    {
        let levels =
        [
            LoggerLevel.ALL,
            LoggerLevel.CRITICAL,
            LoggerLevel.DEBUG,
            LoggerLevel.ERROR,
            LoggerLevel.INFO,
            LoggerLevel.NONE,
            LoggerLevel.WARNING,
            LoggerLevel.WTF
        ];
        return levels.indexOf( level ) > -1 ;
    }}
});

function LoggerEntry( message , level , channel )
{
    this.channel = channel ;
    this.level = level instanceof LoggerLevel ? level : LoggerLevel.ALL ;
    this.message = message ;
}
LoggerEntry.prototype = Object.create( Object.prototype );
LoggerEntry.prototype.constructor = LoggerEntry;

function Logger( channel )
{
    Signal.call( this ) ;
    Object.defineProperties( this ,
    {
        _entry : { value : new LoggerEntry(null,null,channel) , writable : true }
    }) ;
}
Logger.prototype = Object.create( Signal.prototype ,
{
    constructor : { writable : true , value : Logger } ,
    channel : { get : function() { return this._entry.channel ; } },
    critical : { value : function ( context , ...options )
    {
        this._log( LoggerLevel.CRITICAL , context , options ) ;
    }},
    debug : { value : function ( context , ...options )
    {
        this._log( LoggerLevel.DEBUG , context , options ) ;
    }},
    error : { value : function ( context , ...options )
    {
        this._log( LoggerLevel.ERROR , context , options ) ;
    }},
    info : { value : function ( context , ...options )
    {
        this._log( LoggerLevel.INFO , context , options ) ;
    }},
    log : { value : function ( context , ...options )
    {
        this._log( LoggerLevel.ALL , context , options ) ;
    }},
    warning : { value : function ( context , ...options )
    {
        this._log( LoggerLevel.WARNING , context , options ) ;
    }},
    wtf : { value : function ( context , ...options )
    {
        this._log( LoggerLevel.WTF , context , options ) ;
    }},
    toString : { value : function() { return '[Logger]' ; } } ,
    _log : { value : function ( level                 , context , options  )
    {
        if( this.connected() )
        {
            if ( ( typeof(context) === "string" || context instanceof String ) && options instanceof Array )
            {
                var len = options.length;
                for( var i = 0 ; i<len ; i++ )
                {
                    context = String(context).replace( new RegExp( "\\{" + i + "\\}" , "g" ) , options[i] ) ;
                }
            }
            this._entry.message = context ;
            this._entry.level   = level ;
            this.emit( this._entry ) ;
        }
    }}
});

function fastformat( pattern , ...args )
{
    if( (pattern === null) || !(pattern instanceof String || typeof(pattern) === 'string' ) )
    {
        return "" ;
    }
    if( args.length > 0 )
    {
        args = [].concat.apply( [] , args ) ;
        let len  = args.length;
        for( var i = 0 ; i < len ; i++ )
        {
            pattern = pattern.replace( new RegExp( "\\{" + i + "\\}" , "g" ), args[i] );
        }
    }
    return pattern;
}

function InvalidFilterError( message , fileName , lineNumber )
{
    this.name = 'InvalidFilterError';
    this.message    = message || 'invalid filter error' ;
    this.fileName   = fileName ;
    this.lineNumber = lineNumber ;
    this.stack      = (new Error()).stack;
}
InvalidFilterError.prototype = Object.create( Error.prototype );
InvalidFilterError.prototype.constructor = InvalidFilterError;

var strings = Object.defineProperties( {} ,
{
    CHARS_INVALID : { value : "The following characters are not valid\: []~$^&\/(){}<>+\=_-`!@#%?,\:;'\\" , enumerable : true } ,
    CHAR_PLACEMENT : { value : "'*' must be the right most character." , enumerable : true } ,
    EMPTY_FILTER : { value : "filter must not be null or empty." , enumerable : true },
    ERROR_FILTER : { value : "Error for filter '{0}'." , enumerable : true },
    DEFAULT_CHANNEL : { value : "" , enumerable : true },
    ILLEGALCHARACTERS : { value : "[]~$^&/\\(){}<>+=`!#%?,:;'\"@" , enumerable : true },
    INVALID_CHARS : { value : "Channels can not contain any of the following characters : []~$^&/\\(){}<>+=`!#%?,:;'\"@" , enumerable : true },
    INVALID_LENGTH : { value : "Channels must be at least one character in length." , enumerable : true },
    INVALID_TARGET : { value : "Log, Invalid target specified." , enumerable : true }
});

function LoggerTarget()
{
    Object.defineProperties( this ,
    {
        _count   : { value : 0 , writable : true } ,
        _factory : { value : null , writable : true } ,
        _filters : { value : ["*"] , writable : true } ,
        _level   : { value : LoggerLevel.ALL , writable : true }
    }) ;
    this.factory = Log ;
}
LoggerTarget.prototype = Object.create( Receiver.prototype ,
{
    constructor : { value : LoggerTarget , writable : true } ,
    factory :
    {
        get : function() { return this._factory ; },
        set : function( factory )
        {
            if ( this._factory )
            {
                this._factory.removeTarget( this ) ;
            }
            this._factory = ( factory instanceof LoggerFactory ) ? factory : Log ;
            this._factory.addTarget( this ) ;
        }
    },
    filters :
    {
        get : function() { return [].concat( this._filters ) ; },
        set : function( value )
        {
            let filters  = [];
            if ( value && value instanceof Array && value.length > 0 )
            {
                let filter;
                let length = value.length;
                for ( var i = 0 ; i < length ; i++ )
                {
                    filter = value[i] ;
                    if ( filters.indexOf( filter ) === -1 )
                    {
                        this._checkFilter( filter ) ;
                        filters.push( filter ) ;
                    }
                }
            }
            else
            {
                filters.push( '*' ) ;
            }
            if ( this._count > 0 )
            {
                this._factory.removeTarget( this ) ;
            }
            this._filters = filters ;
            if( this._count > 0 )
            {
                this._factory.addTarget( this ) ;
            }
        }
    },
    level :
    {
        get : function() { return this._level ; },
        set : function( value )
        {
            this._factory.removeTarget( this ) ;
            this._level = value || LoggerLevel.ALL ;
            this._factory.addTarget( this ) ;
        }
    },
    addFilter : { value : function ( channel )
    {
        this._checkFilter( channel ) ;
        let index = this._filters.indexOf( channel );
        if ( index === -1 )
        {
            this._filters.push( channel ) ;
            return true ;
        }
        return false ;
    }},
    addLogger : { value : function ( logger )
    {
        if ( logger && logger instanceof Logger )
        {
            this._count ++ ;
            logger.connect( this ) ;
        }
    }},
    logEntry : { value : function( entry )
    {
    }},
    receive : { value : function( entry )
    {
        if ( entry instanceof LoggerEntry )
        {
            if ( this._level === LoggerLevel.NONE )
            {
                return ;
            }
            else if ( entry.level.valueOf() >= this._level.valueOf() )
            {
                this.logEntry( entry ) ;
            }
        }
    }},
    removeFilter : { value : function( channel )
    {
        if ( channel && (typeof(channel) === "string" || (channel instanceof String) ) && ( channel !== "" ) )
        {
            let index = this._filters.indexOf( channel );
            if ( index > -1 )
            {
                this._filters.splice( index , 1 ) ;
                return true ;
            }
        }
        return false ;
    }},
    removeLogger : { value : function( logger )
    {
        if ( logger instanceof Logger )
        {
            this._count-- ;
            logger.disconnect( this ) ;
        }
    }},
    _checkFilter : { value : function( filter )
    {
        if ( filter === null )
        {
            throw new InvalidFilterError( strings.EMPTY_FILTER  ) ;
        }
        if ( this._factory.hasIllegalCharacters( filter ) )
        {
             throw new InvalidFilterError( fastformat( strings.ERROR_FILTER , filter ) + strings.CHARS_INVALID ) ;
        }
        var index  = filter.indexOf("*");
        if ( (index >= 0) && (index !== (filter.length -1)) )
        {
            throw new InvalidFilterError( fastformat( strings.ERROR_FILTER , filter) + strings.CHAR_PLACEMENT ) ;
        }
    }},
    toString : { value : function() { return '[LoggerTarget]' ; } }
});

function LoggerFactory()
{
    Object.defineProperties( this ,
    {
        _loggers     : { value : new ArrayMap()    , writable : true } ,
        _targetLevel : { value : LoggerLevel.NONE  , writable : true } ,
        _targets     : { value : []                , writable : true }
    }) ;
}
LoggerFactory.prototype = Object.create( Receiver.prototype ,
{
    constructor : { value : LoggerFactory } ,
    addTarget : { value : function( target                  )
    {
        if( target && (target instanceof LoggerTarget) )
        {
            let channel;
            let log;
            let filters  = target.filters;
            let it = this._loggers.iterator();
            while ( it.hasNext() )
            {
                log     = it.next() ;
                channel = it.key() ;
                if( this._channelMatchInFilterList( channel, filters ) )
                {
                    target.addLogger( log ) ;
                }
            }
            this._targets.push( target );
            if ( ( this._targetLevel === LoggerLevel.NONE ) || ( target.level.valueOf() < this._targetLevel.valueOf() ) )
            {
                this._targetLevel = target.level ;
            }
        }
        else
        {
            throw new Error( strings.INVALID_TARGET );
        }
    }},
    flush : { value : function()
    {
        this._loggers.clear() ;
        this._targets     = [] ;
        this._targetLevel = LoggerLevel.NONE ;
    }},
    getLogger : { value : function ( channel  )
    {
        this._checkChannel( channel ) ;
        let logger = this._loggers.get( channel );
        if( !logger )
        {
            logger = new Logger( channel ) ;
            this._loggers.set( channel , logger ) ;
        }
        let target;
        let len = this._targets.length;
        for( var i = 0 ; i<len ; i++ )
        {
            target = this._targets[i] ;
            if( this._channelMatchInFilterList( channel , target.filters ) )
            {
                target.addLogger( logger ) ;
            }
        }
        return logger ;
    }},
    hasIllegalCharacters : { value : function ( value )
    {
        return indexOfAny( value , strings.ILLEGALCHARACTERS.split("") ) !== -1 ;
    }},
    isAll : { value : function () { return this._targetLevel === LoggerLevel.ALL ; } },
    isCritical : { value : function () { return this._targetLevel === LoggerLevel.CRITICAL ; } },
    isDebug : { value : function() { return this._targetLevel === LoggerLevel.DEBUG ; } },
    isError : { value : function () { return this._targetLevel === LoggerLevel.ERROR ; } },
    isInfo : { value : function() { return this._targetLevel === LoggerLevel.INFO ; } },
    isWarning : { value : function() { return this._targetLevel === LoggerLevel.WARNING ; } },
    isWtf : { value : function() { return this._targetLevel === LoggerLevel.WTF ; } },
    removeTarget : { value : function ( target )
    {
        if( target && target instanceof LoggerTarget )
        {
            var log;
            var filters = target.filters;
            var it = this._loggers.iterator();
            while ( it.hasNext() )
            {
                log = it.next() ;
                var c = it.key();
                if( this._channelMatchInFilterList( c, filters ) )
                {
                    target.removeLogger( log );
                }
            }
            var len = this._targets.length;
            for( var i = 0  ; i < len ; i++ )
            {
                if( target === this._targets[i] )
                {
                    this._targets.splice(i, 1) ;
                    i-- ;
                }
            }
            this._resetTargetLevel() ;
        }
        else
        {
            throw new Error( strings.INVALID_TARGET );
        }
    }},
    toString : { value : function() { return '[LoggerFactory]' ; } },
    _channelMatchInFilterList : { value : function( channel  , filters  )
    {
        let filter;
        let index = -1;
        let len = filters.length;
        for( var i  = 0 ; i<len ; i++ )
        {
            filter = filters[i] ;
            index  = filter.indexOf("*") ;
            if( index === 0 )
            {
                return true ;
            }
            index = (index < 0) ? ( index = channel.length ) : ( index - 1 ) ;
            if( channel.substring(0, index) === filter.substring(0, index) )
            {
                return true ;
            }
        }
        return false ;
    }},
    _checkChannel : { value : function( channel  )
    {
        if( channel === null || channel.length === 0 )
        {
            throw new InvalidChannelError( strings.INVALID_LENGTH );
        }
        if( this.hasIllegalCharacters( channel ) || ( channel.indexOf("*") !== -1 ) )
        {
            throw new InvalidChannelError( strings.INVALID_CHARS ) ;
        }
    }},
    _resetTargetLevel : { value : function()
    {
        let t;
        let min = LoggerLevel.NONE;
        let len = this._targets.length;
        for ( let i = 0 ; i < len ; i++ )
        {
            t = this._targets[i] ;
            if ( ( min === LoggerLevel.NONE ) || ( t.level.valueOf() < min.valueOf() ) )
            {
                min = t.level ;
            }
        }
        this._targetLevel = min ;
    }}
});

var Log = new LoggerFactory();

var logger = Log.getLogger( "system.ioc.logger" );

var ObjectOrder = Object.defineProperties( {} ,
{
    AFTER : { value : "after" , enumerable : true },
    BEFORE : { value : "before" , enumerable : true },
    NONE : { value : "none" , enumerable : true },
    NOW : { value : "now" , enumerable : true }
});

function ObjectListener( dispatcher , type , method = null , useCapture = false , order = "after" , priority = 0 )
{
    Object.defineProperties( this ,
    {
        dispatcher : { value : dispatcher , writable : true } ,
        method : { value : method , writable : true } ,
        priority : { value : priority , writable : true } ,
        type : { value : type , writable : true } ,
        useCapture : { value : useCapture === true , writable : true } ,
        _order : { value : ( order === ObjectOrder.BEFORE ) ? ObjectOrder.BEFORE : ObjectOrder.AFTER  , writable : true }
    }) ;
}
ObjectListener.prototype = Object.create( Object.prototype ,
{
    constructor : { value : ObjectListener } ,
    order :
    {
        get : function() { return this._order ; } ,
        set : function( value )
        {
            this._order = ( value === ObjectOrder.BEFORE ) ? ObjectOrder.BEFORE : ObjectOrder.AFTER ;
        }
    },
    toString : { value : function () { return '[ObjectListener]' ; }}
}) ;
Object.defineProperties( ObjectListener ,
{
    DISPATCHER : { value : "dispatcher" , enumerable : true } ,
    METHOD : { value : "method" , enumerable : true } ,
    ORDER : { value : "order" , enumerable : true } ,
    PRIORITY : { value : "priority" , enumerable : true } ,
    USE_CAPTURE : { value : "useCapture" , enumerable : true } ,
    TYPE : { value : "type" , enumerable : true }
});

function createListeners( factory )
{
    if ( !factory )
    {
        return null ;
    }
    let a = null;
    if ( factory instanceof Array )
    {
        a = factory ;
    }
    else if( ( ObjectAttribute.LISTENERS in factory ) && (factory[ ObjectAttribute.LISTENERS ] instanceof Array ) )
    {
        a = factory[ ObjectAttribute.LISTENERS ] ;
    }
    if ( a === null || a.length === 0 )
    {
        return null ;
    }
    let def;
    let dispatcher;
    let type;
    let listeners = [];
    let id = String(factory[ ObjectAttribute.ID ]);
    let len = a.length;
    for ( let i = 0 ; i<len ; i++ )
    {
        def = a[i] ;
        if ( def !== null && (ObjectListener.DISPATCHER in def) && (ObjectListener.TYPE in def) )
        {
            dispatcher = def[ ObjectListener.DISPATCHER ] ;
            if ( !(dispatcher instanceof String || typeof(dispatcher) === 'string') || dispatcher.length === 0 )
            {
                continue ;
            }
            type = def[ ObjectListener.TYPE ] ;
            if ( !(type instanceof String || typeof(type) === 'string') || type.length === 0 )
            {
                continue ;
            }
            listeners.push
            (
                new ObjectListener
                (
                    dispatcher , type ,
                    def[ ObjectListener.METHOD ] ,
                    def[ ObjectListener.USE_CAPTURE ] === true ,
                    (def[ ObjectListener.ORDER ] === ObjectOrder.BEFORE) ? ObjectOrder.BEFORE : ObjectOrder.AFTER ,
                    isNaN(def[ ObjectListener.PRIORITY ]) ? 0 : def[ ObjectListener.PRIORITY ]
                )
            ) ;
        }
        else
        {
            if( logger )
            {
                logger.warning
                (
                    "ObjectBuilder.createListeners failed, a listener definition is invalid in the object definition \"{0}\" at \"{1}\" with the value : {2}" ,
                    id , i , dump( def )
                ) ;
            }
        }
    }
    return ( listeners.length > 0 ) ? listeners : null ;
}

function ObjectStrategy() {}
ObjectStrategy.prototype = Object.create( Object.prototype ,
{
    constructor : { writable : true , value : ObjectStrategy },
    toString : { writable : true , value : function () { return '[' + this.constructor.name + ']' ; } }
}) ;

function ObjectProperty( name , value , policy = "value" , evaluators = null )
{
    Object.defineProperties( this ,
    {
        args : { value : null, writable : true } ,
        evaluators : { value : evaluators instanceof Array ? evaluators : null, writable : true } ,
        name : { value : name , writable : true } ,
        scope : { value : null, writable : true } ,
        value : { value : value , writable : true } ,
        _policy : { value : null , writable : true }
    }) ;
    this.policy = policy ;
}
ObjectProperty.prototype = Object.create( ObjectStrategy.prototype ,
{
    constructor : { writable : true , value : ObjectProperty } ,
    policy :
    {
        get : function() { return this._policy ; } ,
        set : function( str )
        {
            switch( str )
            {
                case ObjectAttribute.ARGUMENTS :
                case ObjectAttribute.CALLBACK  :
                case ObjectAttribute.CONFIG    :
                case ObjectAttribute.LOCALE    :
                case ObjectAttribute.REFERENCE :
                {
                    this._policy = str ;
                    break ;
                }
                default :
                {
                    this._policy = ObjectAttribute.VALUE ;
                }
            }
        }
    }
}) ;

function createProperties( factory )
{
    if ( !factory )
    {
        return null ;
    }
    let a = null;
    if ( factory instanceof Array )
    {
        a = factory ;
    }
    else if( (ObjectAttribute.PROPERTIES in factory) && (factory[ObjectAttribute.PROPERTIES] instanceof Array ) )
    {
        a = factory[ObjectAttribute.PROPERTIES] ;
    }
    if ( !(a instanceof Array) || (a.length === 0) )
    {
        return null ;
    }
    let properties = [];
    let id = String(factory[ ObjectAttribute.ID ]);
    let len = a.length;
    let prop = null;
    for ( let i = 0 ; i<len ; i++ )
    {
        prop = a[i] ;
        let args = null;
        let call = null;
        let conf = null;
        let i18n = null;
        let name = null;
        let ref  = null;
        let value = null;
        let evaluators = null;
        if ( prop && (ObjectAttribute.NAME in prop) )
        {
            name = prop[ ObjectAttribute.NAME ] ;
            if ( !(name instanceof String || typeof(name) === 'string') || (name.length === '') )
            {
                continue ;
            }
            if( ObjectAttribute.EVALUATORS in prop )
            {
                evaluators = (prop[ ObjectAttribute.EVALUATORS ] instanceof Array) ? prop[ ObjectAttribute.EVALUATORS ] : null ;
            }
            if( ObjectAttribute.ARGUMENTS in prop )
            {
                args = prop[ ObjectAttribute.ARGUMENTS ] || null ;
            }
            if( ObjectAttribute.CONFIG in prop )
            {
                conf = prop[ ObjectAttribute.CONFIG ] || null ;
            }
            if( ObjectAttribute.LOCALE in prop )
            {
                i18n = prop[ ObjectAttribute.LOCALE ] || null ;
            }
            if( ObjectAttribute.CALLBACK in prop )
            {
                call = prop[ ObjectAttribute.CALLBACK ] ;
            }
            if( ObjectAttribute.REFERENCE in prop )
            {
                ref = prop[ ObjectAttribute.REFERENCE ] || null ;
            }
            if( ObjectAttribute.VALUE in prop )
            {
                value = prop[ ObjectAttribute.VALUE ] ;
            }
            let property = null;
            if ( (ref instanceof String || typeof(ref) === 'string') && (ref !== '') )
            {
                property = new ObjectProperty( name , ref , ObjectAttribute.REFERENCE , evaluators ) ;
            }
            else if ( (conf instanceof String || typeof(conf) === 'string') && (conf !== '') )
            {
                property = new ObjectProperty( name , conf , ObjectAttribute.CONFIG , evaluators ) ;
            }
            else if ( (i18n instanceof String || typeof(i18n) === 'string') && (i18n !== '') )
            {
                property = new ObjectProperty( name , i18n , ObjectAttribute.LOCALE , evaluators ) ;
            }
            else if( call instanceof Function || ( (call instanceof String || typeof(call) === 'string') && (call !== '') ) )
            {
                property = new ObjectProperty( name , call , ObjectAttribute.CALLBACK , evaluators ) ;
                if( args && (args instanceof Array) )
                {
                    property.args = createArguments( args );
                }
                if( ObjectAttribute.SCOPE in prop )
                {
                    property.scope = prop[ ObjectAttribute.SCOPE ] || null ;
                }
            }
            else if ( args && (args instanceof Array) )
            {
                property = new ObjectProperty( name , createArguments( args ) , ObjectAttribute.ARGUMENTS ) ;
            }
            else
            {
                property = new ObjectProperty( name , value , ObjectAttribute.VALUE , evaluators ) ;
            }
            if( property )
            {
                properties.push( property ) ;
            }
        }
        else if( logger )
        {
            logger.warning
            (
                "createProperties failed, a property definition is invalid in the object definition \"{0}\" at \"{1}\" with the value : {2}" ,
                id , i , dump( prop )
            ) ;
        }
    }
    return ( properties.length > 0 ) ? properties : null ;
}

function ObjectReceiver( signal , slot = null , priority = 0 , autoDisconnect = false , order = "after" )
{
    Object.defineProperties( this ,
    {
        autoDisconnect : { value : autoDisconnect , writable : true } ,
        order :
        {
            get : function() { return this._order ; } ,
            set : function( value )
            {
                this._order = ( value === ObjectOrder.BEFORE ) ? ObjectOrder.BEFORE : ObjectOrder.AFTER ;
            }
        },
        priority : { value : priority , writable : true } ,
        signal : { value : signal , writable : true } ,
        slot : { value : slot , writable : true } ,
        _order : { value : ( order === ObjectOrder.BEFORE ) ? ObjectOrder.BEFORE : ObjectOrder.AFTER  , writable : true }
    }) ;
}
ObjectReceiver.prototype = Object.create( Object.prototype ,
{
    constructor : { value : ObjectReceiver },
    toString : { value : function () { return '[ObjectReceiver]' ; }}
});
Object.defineProperties( ObjectReceiver ,
{
    AUTO_DISCONNECT : { value : "autoDisconnect" , enumerable : true } ,
    ORDER : { value : "order" , enumerable : true } ,
    PRIORITY : { value : "priority" , enumerable : true } ,
    SIGNAL : { value : "signal" , enumerable : true } ,
    SLOT : { value : "slot" , enumerable : true }
});

function createReceivers( factory )
{
    if ( !factory )
    {
        return null ;
    }
    let a = null;
    if ( factory instanceof Array )
    {
        a = factory ;
    }
    else if( ( ObjectAttribute.RECEIVERS in factory ) && (factory[ObjectAttribute.RECEIVERS] instanceof Array ) )
    {
        a = factory[ObjectAttribute.RECEIVERS] ;
    }
    if ( a === null || a.length === 0 )
    {
        return null ;
    }
    let def;
    let receivers = [];
    let signal;
    let id = String( factory[ObjectAttribute.ID] );
    let len = a.length;
    for ( let i = 0 ; i<len ; i++ )
    {
        def = a[i] ;
        if ( def !== null && ( ObjectReceiver.SIGNAL in def ) )
        {
            signal = def[ ObjectReceiver.SIGNAL ] ;
            if ( !(signal instanceof String || typeof(signal) === 'string') || signal.length === 0 )
            {
                continue ;
            }
            receivers.push
            (
                new ObjectReceiver
                (
                    signal ,
                    def[ ObjectReceiver.SLOT ] ,
                    isNaN(def[ ObjectReceiver.PRIORITY ]) ? 0 : def[ ObjectReceiver.PRIORITY ] ,
                    def[ ObjectReceiver.AUTO_DISCONNECT ] === true ,
                    ( def[ ObjectReceiver.ORDER ] === ObjectOrder.BEFORE ) ? ObjectOrder.BEFORE : ObjectOrder.AFTER
                )
            ) ;
        }
        else
        {
            logger.warning
            (
                "ObjectBuilder.createReceivers failed, a receiver definition is invalid in the object definition \"{0}\" at \"{1}\" with the value : {2}" ,
                id , i , dump( def )
            ) ;
        }
    }
    return ( receivers.length > 0 ) ? receivers : null ;
}

var ObjectStrategies = Object.defineProperties( {} ,
{
    FACTORY_METHOD : { value : 'factoryMethod' , enumerable : true },
    FACTORY_PROPERTY : { value : 'factoryProperty' , enumerable : true },
    FACTORY_REFERENCE : { value : 'factoryReference' , enumerable : true },
    FACTORY_VALUE : { value : 'factoryValue' , enumerable : true },
    STATIC_FACTORY_METHOD : { value : 'staticFactoryMethod' , enumerable : true },
    STATIC_FACTORY_PROPERTY : { value : 'staticFactoryProperty' , enumerable : true },
});

function ObjectMethod( name , args )
{
    Object.defineProperties( this ,
    {
        args : { value : args , writable : true } ,
        name : { value : name , writable : true }
    }) ;
}
ObjectMethod.prototype = Object.create( ObjectStrategy.prototype ,
{
    constructor : { writable : true , value : ObjectMethod }
}) ;

function ObjectFactoryMethod( factory , name , args )
{
    ObjectMethod.call( this , name , args ) ;
    Object.defineProperties( this ,
    {
        factory : { value : factory , writable : true }
    }) ;
}
ObjectFactoryMethod.prototype = Object.create( ObjectMethod.prototype ,
{
    constructor : { writable : true  , value : ObjectFactoryMethod }
});
Object.defineProperties( ObjectFactoryMethod ,
{
    build : { value : function( o )
    {
        if ( o === null )
        {
            return null ;
        }
        if ( (ObjectAttribute.FACTORY in o) && (ObjectAttribute.NAME in o) )
        {
            return new ObjectFactoryMethod
            (
                o[ ObjectAttribute.FACTORY ] || null ,
                o[ ObjectAttribute.NAME ] || null ,
                createArguments( o[ ObjectAttribute.ARGUMENTS ] || null )
            ) ;
        }
        else
        {
            return null ;
        }
    }}
});

function ObjectFactoryProperty( factory  , name  , evaluators  = null )
{
    ObjectProperty.call( this , name , null, null, evaluators ) ;
    Object.defineProperties( this ,
    {
        factory : { value : factory , writable : true }
    }) ;
}
ObjectFactoryProperty.prototype = Object.create( ObjectProperty.prototype ,
{
    constructor : { writable : true , value : ObjectFactoryProperty }
}) ;
Object.defineProperties( ObjectFactoryProperty ,
{
    build :
    {
        value : function( o )
        {
            if ( o === null )
            {
                return null ;
            }
            if ( (ObjectAttribute.FACTORY in o) && (ObjectAttribute.NAME in o) )
            {
                return new ObjectFactoryProperty
                (
                    o[ ObjectAttribute.FACTORY    ] || null ,
                    o[ ObjectAttribute.NAME       ] || null ,
                    o[ ObjectAttribute.EVALUATORS ] || null
                ) ;
            }
            else
            {
                return null ;
            }
        }
    }
});

function ObjectReference( ref )
{
    Object.defineProperties( this ,
    {
        ref : { value : (ref instanceof String) || typeof(ref) === 'string' ? ref : null , writable : true }
    }) ;
}
ObjectReference.prototype = Object.create( ObjectStrategy.prototype ,
{
    constructor : { value : ObjectReference }
}) ;

function ObjectStaticFactoryMethod( type , name , args )
{
    ObjectMethod.call( this , name , args ) ;
    Object.defineProperties( this ,
    {
        type : { value : type , writable : true }
    }) ;
}
ObjectStaticFactoryMethod.prototype = Object.create( ObjectMethod.prototype ,
{
    constructor : { value : ObjectStaticFactoryMethod , writable : true }
}) ;
Object.defineProperties( ObjectStaticFactoryMethod ,
{
    build : { value : function( o )
    {
        if ( o === null )
        {
            return null ;
        }
        if ( ( ObjectAttribute.TYPE in o ) && ( ObjectAttribute.NAME in o ) )
        {
            let strategy = new ObjectStaticFactoryMethod
            (
                o[ ObjectAttribute.TYPE ] || null ,
                o[ ObjectAttribute.NAME ] || null ,
                createArguments( o[ ObjectAttribute.ARGUMENTS ] || null )
            );
            return strategy ;
        }
        else
        {
            return null ;
        }
    }}
});

function ObjectStaticFactoryProperty( name  , type  , evaluators  = null )
{
    ObjectProperty.call( this , name , null, null, evaluators ) ;
    Object.defineProperties( this ,
    {
        type : { value : type , writable : true }
    }) ;
}
ObjectStaticFactoryProperty.prototype = Object.create( ObjectProperty.prototype ,
{
    constructor : { value : ObjectStaticFactoryProperty , writable : true }
}) ;
Object.defineProperties( ObjectStaticFactoryProperty ,
{
    build : { value : function( o )
    {
        if ( o === null )
        {
            return null ;
        }
        if ( (ObjectAttribute.TYPE in o) && (ObjectAttribute.NAME in o) )
        {
            return new ObjectStaticFactoryProperty
            (
                o[ ObjectAttribute.NAME       ] || null ,
                o[ ObjectAttribute.TYPE       ] || null ,
                o[ ObjectAttribute.EVALUATORS ] || null
            ) ;
        }
        else
        {
            return null ;
        }
    }}
});

function ObjectValue( value )
{
    Object.defineProperties( this ,
    {
        value : { writable : true , value : value }
    }) ;
}
ObjectValue.prototype = Object.create( ObjectStrategy.prototype ,
{
    constructor : { writable : true , value : ObjectValue }
}) ;

function createStrategy( o )
{
    if ( ObjectStrategies.FACTORY_METHOD in o )
    {
        return ObjectFactoryMethod.build( o[ ObjectStrategies.FACTORY_METHOD ] ) ;
    }
    else if ( ObjectStrategies.FACTORY_PROPERTY in o )
    {
        return ObjectFactoryProperty.build( o[ ObjectStrategies.FACTORY_PROPERTY ] ) ;
    }
    else if ( ObjectStrategies.FACTORY_REFERENCE in o )
    {
        return new ObjectReference( o[ ObjectStrategies.FACTORY_REFERENCE ] ) ;
    }
    else if ( ObjectStrategies.FACTORY_VALUE in o )
    {
        return new ObjectValue( o[ ObjectStrategies.FACTORY_VALUE ] ) ;
    }
    else if ( ObjectStrategies.STATIC_FACTORY_METHOD  in o )
    {
        return ObjectStaticFactoryMethod.build( o[ ObjectStrategies.STATIC_FACTORY_METHOD ] ) ;
    }
    else if ( ObjectStrategies.STATIC_FACTORY_PROPERTY in o )
    {
        return ObjectStaticFactoryProperty.build( o[ ObjectStrategies.STATIC_FACTORY_PROPERTY ] ) ;
    }
    else
    {
        return null ;
    }
}

var ObjectScope = Object.defineProperties( {} ,
{
    PROTOTYPE : { value : "prototype" , enumerable : true },
    SINGLETON : { value : "singleton" , enumerable : true },
    SCOPES : { value : [ "prototype" , "singleton" ] },
    validate : { value : function( scope )
    {
        return ObjectScope.SCOPES.indexOf( scope ) > -1 ;
    }}
});

function ObjectDefinition( id , type , singleton = false , lazyInit = false , lazyType=false )
{
    if ( id === null || id === undefined )
    {
        throw new ReferenceError( this + " constructor failed, the 'id' value passed in argument not must be empty or 'null' or 'undefined'.") ;
    }
    if ( type === null || type === undefined )
    {
        throw new ReferenceError( this + " constructor failed, the 'type' passed in argument not must be empty or 'null' or 'undefined'.") ;
    }
    Object.defineProperties( this ,
    {
        constructorArguments : { value : null , enumerable : true , writable : true } ,
        destroyMethodName : { value : null , enumerable : true , writable : true } ,
        id : { value : id , enumerable : true , writable : true } ,
        identify : { value : false , enumerable : true , writable : true } ,
        initMethodName : { value : null , enumerable : true , writable : true } ,
        lock : { value : false , enumerable : true , writable : true } ,
        properties : { value : null , enumerable : true , writable : true } ,
        type : { value : type , enumerable : true , writable : true } ,
        _afterListeners  : { value : null , writable : true } ,
        _beforeListeners : { value : null , writable : true } ,
        _dependsOn       : { value : null , writable : true } ,
        _generates       : { value : null , writable : true } ,
        _lazyInit        : { value : lazyInit && singleton , writable : true } ,
        _lazyType        : { value : lazyType === true , writable : true } ,
        _singleton       : { value : singleton === true , writable : true } ,
        _scope           : { value : singleton === true ? ObjectScope.SINGLETON : ObjectScope.PROTOTYPE , writable : true } ,
        _strategy        : { value : null , writable : true }
    }) ;
}
ObjectDefinition.prototype = Object.create( Identifiable.prototype ,
{
    constructor : { writable : true , value : Identifiable },
    afterListeners : { get : function() { return this._afterListeners ; } },
    afterReceivers : { get : function() { return this._afterReceivers ; } },
    beforeListeners : { get : function() { return this._beforeListeners ; } },
    beforeReceivers : { get : function() { return this._beforeReceivers ; } },
    dependsOn :
    {
        get : function() { return this._dependsOn ; } ,
        set : function( ar )
        {
            this._dependsOn = ( ar instanceof Array && ar.length > 0 ) ? ar.filter( this._filterStrings ) : null ;
        }
    },
    generates :
    {
        get : function() { return this._generates ; } ,
        set : function( ar )
        {
            this._generates = ( ar instanceof Array && ar.length > 0 ) ? ar.filter( this._filterStrings ) : null ;
        }
    },
    lazyInit :
    {
        get : function() { return this._lazyInit; },
        set : function( flag )
        {
            this._lazyInit = ((flag instanceof Boolean) || (typeof(flag) === 'boolean')) ? flag : false ;
        }
    },
    lazyType :
    {
        get : function() { return this._lazyType; },
        set : function( value )
        {
            this._lazyType = value === true ;
        }
    },
    listeners : { set : function( ar )
    {
        this._afterListeners  = [] ;
        this._beforeListeners = [] ;
        if ( ar === null || !(ar instanceof Array) )
        {
            return ;
        }
        let l = ar.length;
        if ( l > 0 )
        {
            for( let i = 0 ; i < l ; i++ )
            {
                let r = ar[i];
                if ( r instanceof ObjectListener )
                {
                    if( r.order === ObjectOrder.AFTER )
                    {
                        this._afterListeners.push( r ) ;
                    }
                    else
                    {
                        this._beforeListeners.push( r ) ;
                    }
                }
            }
        }
    }},
    receivers : { set : function( ar )
    {
        this._afterReceivers  = [] ;
        this._beforeReceivers = [] ;
        if ( ar === null || !(ar instanceof Array) )
        {
            return ;
        }
        let l = ar.length;
        if ( l > 0 )
        {
            for( var i = 0 ; i < l ; i++ )
            {
                let r = ar[i];
                if ( r instanceof ObjectReceiver )
                {
                    if( r.order === ObjectOrder.AFTER )
                    {
                        this._afterReceivers.push( r ) ;
                    }
                    else
                    {
                        this._beforeReceivers.push( r ) ;
                    }
                }
            }
        }
    }},
    singleton : { get : function() { return this._singleton; } },
    scope :
    {
        get : function() { return this._scope ; } ,
        set : function( scope )
        {
            this._scope = ObjectScope.validate( scope ) ? scope : ObjectScope.PROTOTYPE ;
            this._singleton = Boolean(this._scope === ObjectScope.SINGLETON) ;
        }
    },
    strategy :
    {
        get : function() { return this._strategy ; } ,
        set : function( strategy )
        {
            this._strategy = (strategy instanceof ObjectStrategy) ? strategy : null ;
        }
    },
    toString : { value : function () { return "[ObjectDefinition]" ; } } ,
    _filterStrings : { value : function( item )
    {
        return (typeof(item) === 'string' || item instanceof String) && item.length > 0 ;
    }}
}) ;

function createObjectDefinition( o )
{
    let definition = new ObjectDefinition
    (
        o[ ObjectAttribute.ID ]        || null ,
        o[ ObjectAttribute.TYPE ]      || null ,
        o[ ObjectAttribute.SINGLETON ] || false ,
        o[ ObjectAttribute.LAZY_INIT ] || false ,
        o[ ObjectAttribute.LAZY_TYPE ] || false
    );
    if( (ObjectAttribute.IDENTIFY in o) && (o[ObjectAttribute.IDENTIFY] instanceof Boolean || typeof(o[ObjectAttribute.IDENTIFY]) === 'boolean') )
    {
        definition.identify = o[ ObjectAttribute.IDENTIFY ] ;
    }
    if( (ObjectAttribute.LOCK in o) && (o[ObjectAttribute.LOCK] instanceof Boolean || typeof(o[ObjectAttribute.LOCK]) === 'boolean') )
    {
        definition.lock = o[ ObjectAttribute.LOCK ] ;
    }
    if( (ObjectAttribute.ARGUMENTS in o ) && ( o[ ObjectAttribute.ARGUMENTS ] instanceof Array ) )
    {
        definition.constructorArguments = createArguments( o[ ObjectAttribute.ARGUMENTS ] );
    }
    if( ObjectAttribute.DESTROY_METHOD_NAME in o )
    {
        definition.destroyMethodName = o[ ObjectAttribute.DESTROY_METHOD_NAME ] ;
    }
    if( ObjectAttribute.INIT_METHOD_NAME in o )
    {
        definition.initMethodName = o[ ObjectAttribute.INIT_METHOD_NAME ] ;
    }
    if( ObjectAttribute.SCOPE in o )
    {
        definition.scope = o[ ObjectAttribute.SCOPE ] ;
    }
    if( (ObjectAttribute.DEPENDS_ON in o) && ( o[ObjectAttribute.DEPENDS_ON] instanceof Array ) )
    {
        definition.dependsOn = o[ ObjectAttribute.DEPENDS_ON ] ;
    }
    if( (ObjectAttribute.GENERATES in o) && (o[ObjectAttribute.GENERATES] instanceof Array) )
    {
        definition.generates = o[ ObjectAttribute.GENERATES ] ;
    }
    let listeners = createListeners( o );
    if( listeners )
    {
        definition.listeners = listeners ;
    }
    let properties = createProperties( o );
    if( properties )
    {
        definition.properties = properties ;
    }
    let receivers = createReceivers( o );
    if( receivers )
    {
        definition.receivers = receivers ;
    }
    let strategy = createStrategy( o );
    if( strategy )
    {
        definition.strategy = strategy ;
    }
    return definition ;
}

var MagicReference = Object.defineProperties( {} ,
{
    CONFIG : { value : "#config" , enumerable : true },
    INIT : { value : "#init" , enumerable : true },
    LOCALE : { value : "#locale" , enumerable : true },
    PARAMS : { value : "#params" , enumerable : true },
    ROOT : { value : "#root" , enumerable : true },
    STAGE : { value : "#stage" , enumerable : true },
    THIS : { value : "#this" , enumerable : true }
});

function Formattable()
{
}
Formattable.prototype = Object.create( Object.prototype );
Formattable.prototype.constructor = Formattable;
Formattable.prototype.format = function( value )
{
};

function ExpressionFormatter()
{
    Object.defineProperties( this ,
    {
        expressions : { value : new ArrayMap() },
        _beginSeparator : { value : '{' , writable : true } ,
        _endSeparator : { value : '}' , writable : true } ,
        _pattern : { value : "{0}((\\w+\)|(\\w+)((.\\w)+|(.\\w+))){1}" } ,
        _reg : { value : null , writable : true }
    }) ;
    this._reset() ;
}
Object.defineProperties( ExpressionFormatter ,
{
    MAX_RECURSION : { value : 200 , enumerable : true }
}) ;
ExpressionFormatter.prototype = Object.create( Formattable.prototype ,
{
    constructor : { value : ExpressionFormatter } ,
    beginSeparator :
    {
        get : function()
        {
            return this._beginSeparator ;
        },
        set : function( str )
        {
            this._beginSeparator = str || "{" ;
            this._reset() ;
        }
    },
    endSeparator :
    {
        get : function()
        {
            return this._endSeparator ;
        },
        set : function( str )
        {
            this._endSeparator = str || "}" ;
            this._reset() ;
        }
    },
    length :
    {
        get : function()
        {
            return this.expressions.length ;
        }
    },
    clear :
    {
        value : function ()
        {
            this.expressions.clear() ;
        }
    },
    format :
    {
        value : function ( value )
        {
            return this._format( String(value) , 0 ) ;
        }
    },
    set :
    {
        value : function ( key , value )
        {
            if ( key === '' || !(key instanceof String || typeof(key) === 'string') )
            {
                return false ;
            }
            if ( value === '' || !(value instanceof String || typeof(value) === 'string') )
            {
                return false ;
            }
            this.expressions.set( key , value ) ;
            return true ;
        }
    },
    toString : { value : function() { return '[ExpressionFormatter]' ; } } ,
    _reset :
    {
        value : function()
        {
            this._reg = new RegExp( fastformat( this._pattern , this.beginSeparator , this.endSeparator ) , "g" ) ;
        }
    },
    _format : { value : function( str , depth = 0 )
    {
        if ( depth >= ExpressionFormatter.MAX_RECURSION )
        {
            return str ;
        }
        let m = str.match( this._reg );
        if ( m === null )
        {
            return str ;
        }
        let l = m.length;
        if ( l > 0 )
        {
            let exp;
            let key;
            for ( let i = 0 ; i<l ; i++ )
            {
                key = m[i].substr(1) ;
                key = key.substr( 0 , key.length-1 ) ;
                if ( this.expressions.has( key ) )
                {
                    exp = this._format( this.expressions.get(key) , depth + 1 ) ;
                    this.expressions.set( key , exp ) ;
                    str = str.replace( m[i] , exp ) || exp ;
                }
            }
        }
        return str ;
    }}
}) ;

function PropertyEvaluator( target )
{
    Object.defineProperties( this ,
    {
        separator : { value : "." , writable : true } ,
        target : { value : target , writable : true , configurable : true } ,
        throwError : { value : false , writable : true } ,
        undefineable : { value : null , writable : true }
    }) ;
}
PropertyEvaluator.prototype = Object.create( Evaluable.prototype );
PropertyEvaluator.prototype.constructor = PropertyEvaluator;
PropertyEvaluator.prototype.eval = function ( o )
{
    if ( o !== null && ( typeof(o) === "string" || o instanceof String ) && (this.target !== null) )
    {
        var exp  = String(o);
        if ( exp.length > 0 )
        {
            var value = this.target;
            var members = exp.split( this.separator );
            var len = members.length;
            for ( var i  = 0 ; i < len ; i++ )
            {
                if ( members[i] in value )
                {
                    value = value[ members[i] ] ;
                }
                else
                {
                    if ( this.throwError )
                    {
                        throw new EvalError( this + " eval failed with the expression : " + o ) ;
                    }
                    return this.undefineable ;
                }
            }
            return value ;
        }
    }
    return this.undefineable ;
};
PropertyEvaluator.prototype.toString = function ()
{
    return "[PropertyEvaluator]" ;
};

function ConfigEvaluator( config )
{
    PropertyEvaluator.call(this) ;
    this.config = (config instanceof ObjectConfig) ? config : null ;
    Object.defineProperties( this ,
    {
        target : { get : function() { return this.config !== null ? this.config.config : null ; } }
    }) ;
}
ConfigEvaluator.prototype = Object.create( PropertyEvaluator.prototype ,
{
    constructor : { value : ConfigEvaluator }
});

function LocaleEvaluator( config )
{
    PropertyEvaluator.call(this) ;
    this.config = (config instanceof ObjectConfig) ? config : null ;
    Object.defineProperties( this ,
    {
        target :
        {
            get : function() { return this.config !== null ? this.config.locale : null ; }
        }
    }) ;
}
LocaleEvaluator.prototype = Object.create( PropertyEvaluator.prototype ,
{
    constructor : { value : LocaleEvaluator }
});

function ReferenceEvaluator( factory )
{
    Object.defineProperties( this ,
    {
        factory : { value : (factory instanceof ObjectFactory) ? factory : null , writable : true } ,
        separator : { value : "." , writable : true } ,
        undefineable : { value : null , writable : true } ,
        throwError :
        {
            get : function() { return this._propEvaluator.throwError ; } ,
            set : function( flag ) { this._propEvaluator.throwError = flag ; }
        },
        _propEvaluator : { value : new PropertyEvaluator() , writable : true }
    }) ;
}
ReferenceEvaluator.prototype = Object.create( Evaluable.prototype ,
{
    constructor : { value : ReferenceEvaluator } ,
    eval : { value : function( o )
    {
        if ( (this.factory instanceof ObjectFactory) && (o instanceof String || typeof(o) === 'string' ) )
        {
            var exp = String(o);
            if ( exp.length > 0 )
            {
                var root;
                try
                {
                    root = this.factory.config.root ;
                }
                catch (e)
                {
                }
                switch( exp )
                {
                    case MagicReference.CONFIG :
                    {
                        return this.factory.config.config ;
                    }
                    case MagicReference.LOCALE :
                    {
                        return this.factory.config.locale ;
                    }
                    case MagicReference.PARAMS :
                    {
                        return this.factory.config.parameters ;
                    }
                    case MagicReference.THIS :
                    {
                        return this.factory ;
                    }
                    case MagicReference.ROOT :
                    {
                        return root ;
                    }
                    case MagicReference.STAGE :
                    {
                        var stage = this.factory.config.stage;
                        if ( stage !== null )
                        {
                            return stage ;
                        }
                        else if ( root && ( "stage" in root ) && ( root.stage !== null ) )
                        {
                             return root.stage ;
                        }
                        else
                        {
                            return this.undefineable ;
                        }
                        break ;
                    }
                    default :
                    {
                        let members = exp.split( this.separator );
                        if ( members.length > 0 )
                        {
                            let ref   = members.shift();
                            let value = this.factory.getObject( ref );
                            if ( value && members.length > 0 )
                            {
                                this._propEvaluator.target = value ;
                                value = this._propEvaluator.eval( members.join(".") ) ;
                                this._propEvaluator.target = null ;
                            }
                            return value ;
                        }
                    }
                }
            }
        }
        return this.undefineable ;
    }}
});

var global = global || null;
if( !global )
{
    try
    {
        global = window ;
    }
    catch( e )
    {
    }
}
if( !global )
{
    try
    {
        global = document ;
    }
    catch( e )
    {
    }
}
if( !global )
{
    global = {} ;
}

function getDefinitionByName( name , domain = null )
{
    if( ( name instanceof String ) || typeof(name) === 'string' )
    {
        name = name.split('.') ;
        if( name.length > 0 )
        {
            try
            {
                var o = domain || global;
                name.forEach( ( element ) =>
                {
                    if(o.hasOwnProperty(element) )
                    {
                        o = o[element] ;
                    }
                    else
                    {
                        return undefined ;
                    }
                });
                return o ;
            }
            catch( e )
            {
            }
        }
    }
    return undefined ;
}

var TypePolicy = Object.defineProperties( {} ,
{
    ALIAS : { value : "alias" , enumerable : true },
    ALL : { value : "all" , enumerable : true },
    EXPRESSION : { value : "expression" , enumerable : true },
    NONE : { value : "none" , enumerable : true }
});

function TypeEvaluator( config = null )
{
    Object.defineProperties( this ,
    {
        config : { value : (config instanceof ObjectConfig) ? config : null , writable : true } ,
        throwError : { value : false , writable : true }
    }) ;
}
TypeEvaluator.prototype = Object.create( Evaluable.prototype ,
{
    constructor : { value : TypeEvaluator } ,
    eval : { value : function( o )
    {
        if ( o instanceof Function )
        {
            return o ;
        }
        else if ( o instanceof String || typeof(o) === 'string' )
        {
            var type   = String(o);
            var config = this.config;
            if ( config && config instanceof ObjectConfig )
            {
                var policy = config.typePolicy;
                if ( policy !== TypePolicy.NONE )
                {
                    if ( policy === TypePolicy.ALL || policy === TypePolicy.ALIAS )
                    {
                        var aliases = config.typeAliases;
                        if ( (aliases instanceof ArrayMap) && aliases.has(type) )
                        {
                            type = aliases.get(type) ;
                        }
                    }
                    if ( policy === TypePolicy.ALL || policy === TypePolicy.EXPRESSION )
                    {
                       if ( config.typeExpression instanceof ExpressionFormatter )
                       {
                           type = config.typeExpression.format(type) ;
                       }
                    }
                }
            }
            try
            {
                var func = getDefinitionByName( type , config.domain );
                if( func instanceof Function )
                {
                    return func ;
                }
            }
            catch( e)
            {
                if ( this.throwError )
                {
                    throw new EvalError( this + " eval failed : " + e.toString() ) ;
                }
            }
        }
        return null ;
    }}
});

function ObjectConfig( init = null )
{
    Object.defineProperties( this ,
    {
        defaultDestroyMethod : { value : null , writable : true , enumerable : true } ,
        defaultInitMethod : { value : null , writable : true , enumerable : true } ,
        domain : { value : null , writable : true , enumerable : true } ,
        identify : { value : false , writable : true , enumerable : true } ,
        lazyInit : { value : false , writable : true , enumerable : true } ,
        lock : { value : false , writable : true , enumerable : true } ,
        parameters : { value : null , writable : true , enumerable : true } ,
        root : { value : null , writable : true , enumerable : true } ,
        stage : { value : null , writable : true , enumerable : true } ,
        useLogger : { value : false , writable : true , enumerable : true } ,
        _config             : { value : {} , writable : true } ,
        _configEvaluator    : { value : new ConfigEvaluator( this ) , writable : true } ,
        _locale             : { value : {} , writable : true } ,
        _localeEvaluator    : { value : new LocaleEvaluator( this ) , writable : true } ,
        _referenceEvaluator : { value : new ReferenceEvaluator() , writable : true } ,
        _typeAliases        : { value : new ArrayMap() , writable : true } ,
        _typeEvaluator      : { value : new TypeEvaluator( this ) , writable : true } ,
        _typeExpression     : { value : new ExpressionFormatter() , writable : true } ,
        _typePolicy         : { value : TypePolicy.NONE , writable : true }
    });
    this.throwError = false ;
    if( init )
    {
        this.initialize( init ) ;
    }
}
Object.defineProperties( ObjectConfig ,
{
    TYPE_ALIAS : { value : 'alias' , enumerable : true }
});
ObjectConfig.prototype = Object.create( Object.prototype ,
{
    constructor : { value : ObjectConfig } ,
    config :
    {
        get : function() { return this._config ; } ,
        set : function( init )
        {
            for( let prop in init )
            {
                this._config[prop] = init[prop] ;
            }
        }
    },
    configEvaluator :
    {
        get : function() { return this._configEvaluator ; }
    },
    locale :
    {
        get : function() { return this._locale ; } ,
        set : function( init )
        {
            for( let prop in init )
            {
                this._locale[prop] = init[prop] ;
            }
        }
    },
    localeEvaluator :
    {
        get : function() { return this._localeEvaluator ; }
    },
    referenceEvaluator :
    {
        get : function() { return this._referenceEvaluator ; }
    },
    throwError :
    {
        get : function() { return this._configEvaluator.throwError && this._localeEvaluator.throwError && this._typeEvaluator.throwError && this._referenceEvaluator.throwError ; } ,
        set : function( flag )
        {
            this._configEvaluator.throwError    = flag ;
            this._localeEvaluator.throwError    = flag ;
            this._referenceEvaluator.throwError = flag ;
            this._typeEvaluator.throwError      = flag ;
        }
    },
    typeAliases :
    {
        get : function() { return this._typeAliases ; } ,
        set : function( aliases )
        {
            if ( aliases instanceof ArrayMap )
            {
                let it = aliases.iterator();
                while( it.hasNext() )
                {
                    let next = it.next();
                    let key  = it.key();
                    this._typeAliases.set(key, next) ;
                }
            }
            else if ( aliases instanceof Array )
            {
                let len = aliases.length;
                if ( len > 0 )
                {
                   while ( --len > -1 )
                   {
                        let item = aliases[len];
                        if ( item !== null && ( ObjectConfig.TYPE_ALIAS in item ) && ( ObjectAttribute.TYPE in item ) )
                        {
                            this._typeAliases.set( String(item[ObjectConfig.TYPE_ALIAS]) , String(item[ObjectAttribute.TYPE]) ) ;
                        }
                   }
                }
            }
        }
    },
    typeEvaluator :
    {
        get : function() { return this._typeEvaluator ; }
    },
    typeExpression :
    {
        get : function() { return this._typeExpression ; } ,
        set : function( expressions                               )
        {
            if ( expressions instanceof ExpressionFormatter )
            {
                this._typeExpression = expressions ;
            }
            else if ( expressions instanceof Array )
            {
                if ( this._typeExpression === null )
                {
                    this._typeExpression = new ExpressionFormatter() ;
                }
                let len = expressions.length;
                if ( len > 0 )
                {
                   while ( --len > -1 )
                   {
                        let item = expressions[len];
                        if ( item !== null && ( ObjectAttribute.NAME in item ) && ( ObjectAttribute.VALUE in item ) )
                        {
                            this._typeExpression.set( String(item[ObjectAttribute.NAME]) , String(item[ObjectAttribute.VALUE]) ) ;
                        }
                   }
                }
            }
            else
            {
                this._typeExpression = new ExpressionFormatter() ;
            }
        }
    },
    typePolicy :
    {
        get : function() { return this._typePolicy ; } ,
        set : function( policy )
        {
            switch( policy )
            {
                case TypePolicy.ALIAS      :
                case TypePolicy.EXPRESSION :
                case TypePolicy.ALL        :
                {
                    this._typePolicy = policy ;
                    break ;
                }
                default :
                {
                    this._typePolicy = TypePolicy.NONE ;
                }
            }
        }
    },
    initialize : { value : function( init )
    {
        if ( init === null )
        {
            return ;
        }
        for (let prop in init)
        {
            if ( prop in this )
            {
                this[prop] = init[prop] ;
            }
        }
    }},
    setConfigTarget : { value : function( o = null )
    {
        this._config = o || {} ;
    }},
    setLocaleTarget : { value : function( o = null )
    {
        this._locale = o || {} ;
    }},
    toString : { value : function() { return '[ObjectConfig]' ; } }
});

function Runnable() {}
Runnable.prototype = Object.create( Object.prototype ,
{
    constructor : { writable : true , value : Runnable },
    run : { writable : true , value : function()
    {
    }},
    toString : { writable : true , value : function()
    {
        return '[' + this.constructor.name + ']' ;
    }}
});

var TaskPhase = Object.defineProperties( {} ,
{
    ERROR : { value : 'error' , enumerable : true } ,
    DELAYED : { value : 'delayed'  , enumerable : true } ,
    FINISHED : { value : 'finished' , enumerable : true } ,
    INACTIVE : { value : 'inactive' , enumerable : true } ,
    RUNNING : { value : 'running' , enumerable : true } ,
    STOPPED : { value : 'stopped' , enumerable : true } ,
    TIMEOUT : { value : 'timeout' , enumerable : true }
});

function Action()
{
    Object.defineProperties( this ,
    {
        finishIt : { value : new Signal() },
        startIt : { value : new Signal() } ,
        __lock__ : { writable : true  , value : false },
        _phase : { writable : true , value : TaskPhase.INACTIVE },
        _running : { writable : true , value : false }
    }) ;
}
Action.prototype = Object.create( Runnable.prototype ,
{
    constructor : { writable : true , value : Action } ,
    phase : { get : function() { return this._phase ; } },
    running : { get : function() { return this._running ; } },
    clone : { writable : true , value : function()
    {
        return new Action() ;
    }},
    isLocked : { writable : true , value : function()
    {
        return this.__lock__ ;
    }},
    lock : { writable : true , value : function()
    {
        this.__lock__ = true ;
    }},
    notifyFinished : { writable : true , value : function()
    {
        this._running = false ;
        this._phase = TaskPhase.FINISHED ;
        this.finishIt.emit( this ) ;
        this._phase = TaskPhase.INACTIVE ;
    }},
    notifyStarted : { writable : true , value : function()
    {
        this._running = true ;
        this._phase  = TaskPhase.RUNNING ;
        this.startIt.emit( this ) ;
    }},
    unlock : { writable : true , value : function()
    {
        this.__lock__ = false ;
    }}
});

function Task()
{
    Action.call( this ) ;
    Object.defineProperties( this ,
    {
        changeIt : { value : new Signal() },
        clearIt : { value : new Signal() },
        errorIt : { value : new Signal() },
        infoIt : { value : new Signal() },
        looping : { value : false  , writable : true } ,
        loopIt : { value : new Signal() },
        pauseIt : { value : new Signal() },
        progressIt : { value : new Signal() },
        resumeIt : { value : new Signal() },
        stopIt : { value : new Signal() },
        throwError : { value : false , writable : true } ,
        timeoutIt : { value : new Signal() }
    }) ;
}
Task.prototype = Object.create( Action.prototype ,
{
    constructor : { writable : true , value : Task },
    clone : { writable : true , value : function()
    {
        return new Task() ;
    }},
    notifyChanged : { writable : true , value : function()
    {
        if ( !this.__lock__ )
        {
            this.changeIt.emit( this ) ;
        }
    }},
    notifyCleared : { writable : true , value : function()
    {
        if ( !this.__lock__ )
        {
            this.clearIt.emit( this ) ;
        }
    }},
    notifyError : { writable : true , value : function( message = null )
    {
        this._running = false ;
        this._phase = TaskPhase.ERROR ;
        if ( !this.__lock__ )
        {
            this.errorIt.emit( this , message ) ;
        }
        if( this.throwError )
        {
            throw new Error( message ) ;
        }
    }},
    notifyInfo : { writable : true , value : function( info )
    {
        if ( !this.__lock__ )
        {
            this.infoIt.emit( this , info ) ;
        }
    }},
    notifyLooped : { writable : true , value : function()
    {
        this._phase = TaskPhase.RUNNING ;
        if ( !this.__lock__ )
        {
            this.loopIt.emit( this ) ;
        }
    }},
    notifyPaused : { writable : true , value : function()
    {
        this._running = false ;
        this._phase = TaskPhase.STOPPED ;
        if ( !this.__lock__ )
        {
            this.pauseIt.emit( this ) ;
        }
    }},
    notifyProgress : { writable : true , value : function()
    {
        if ( !this.__lock__ )
        {
            this.progressIt.emit( this ) ;
        }
    }},
    notifyResumed : { writable : true , value : function()
    {
        this._phase = TaskPhase.RUNNING ;
        if ( !this.__lock__ )
        {
            this.resumeIt.emit( this ) ;
        }
    }},
    notifyStopped : { writable : true , value : function()
    {
        this._running = false ;
        this._phase = TaskPhase.STOPPED ;
        if ( !this.__lock__ )
        {
            this.stopIt.emit( this ) ;
        }
    }},
    notifyTimeout : { writable : true , value : function()
    {
        this._running = false ;
        this._phase = TaskPhase.TIMEOUT ;
        if ( !this.__lock__ )
        {
            this.timeoutIt.emit( this ) ;
        }
    }},
    resume : { writable : true , value : function() {} },
    reset : { writable : true , value : function() {} },
    start : { writable : true , value : function()
    {
        this.run() ;
    }},
    stop : { writable : true , value : function() {} }
});

function ObjectDefinitionContainer()
{
    Task.call(this) ;
    Object.defineProperties( this ,
    {
        _map : { writable : true , value : new ArrayMap() }
    });
}
ObjectDefinitionContainer.prototype = Object.create( Task.prototype ,
{
    constructor : { configurable : true , writable : true , value : ObjectDefinitionContainer } ,
    numObjectDefinition : { get : function() { return this._map.length ; } } ,
    addObjectDefinition : { value : function( definition )
    {
        if ( definition instanceof ObjectDefinition )
        {
            this._map.set( definition.id , definition ) ;
        }
        else
        {
            throw new ReferenceError( this + " addObjectDefinition failed, the specified object definition must be an ObjectDefinition object." ) ;
        }
    }},
    clearObjectDefinition : { value : function()
    {
        this._map.clear() ;
    }},
    clone : { value : function()
    {
        return new ObjectDefinitionContainer() ;
    }},
    getObjectDefinition : { value : function( id )
    {
        if ( this._map.has( id ) )
        {
            return this._map.get( id ) ;
        }
        else
        {
            throw new ReferenceError( this + " getObjectDefinition failed, the specified object definition don't exist : " + id ) ;
        }
    }},
    hasObjectDefinition : { value : function( id )
    {
        return this._map.has( id ) ;
    }},
    removeObjectDefinition : { value : function( id )
    {
        if ( this._map.has( id ) )
        {
            this._map.delete( id ) ;
        }
        else
        {
            throw new ReferenceError( this + " removeObjectDefinition failed, the specified object definition don't exist : " + id ) ;
        }
    }}
});

function ObjectFactory( config = null , objects = null )
{
    ObjectDefinitionContainer.call(this) ;
    Object.defineProperties( this ,
    {
        objects : { value : (objects instanceof Array) ? objects : null , writable : true } ,
        bufferSingletons : { value : [] , writable : true } ,
        _config : { value : null , writable : true } ,
        _evaluator : { value : new MultiEvaluator() } ,
        _singletons : { value : new ArrayMap() }
    }) ;
    this.config = config ;
}
ObjectFactory.prototype = Object.create( ObjectDefinitionContainer.prototype ,
{
    constructor : { value :  ObjectFactory },
    config :
    {
        get : function() { return this._config } ,
        set : function( config )
        {
            if ( this._config )
            {
                this._config.referenceEvaluator.factory = null ;
            }
            this._config = (config instanceof ObjectConfig) ? config : new ObjectConfig() ;
            this._config.referenceEvaluator.factory = this ;
        }
    },
    singletons : { get : function() { return this._singletons ; } } ,
    clone : { value : function()
    {
        return new ObjectFactory( this.config , [].concat( this.objects ) ) ;
    }},
    hasSingleton : { value : function( id )
    {
        return this._singletons.has(id) ;
    }},
    getObject : { value : function( id )
    {
        if ( !(id instanceof String || typeof(id) === 'string') )
        {
           return null ;
        }
        let instance = null;
        try
        {
            let definition;
            try
            {
                definition = this.getObjectDefinition( id ) ;
            }
            catch (e)
            {
            }
            if ( !(definition instanceof ObjectDefinition) )
            {
                throw new Error( "the definition is not register in the factory") ;
            }
            if ( definition.singleton )
            {
                instance = this._singletons.get(id) || null ;
            }
            if ( !instance )
            {
                if( !(definition.type instanceof Function) )
                {
                    if( definition.type instanceof String || typeof(definition.type) === 'string' )
                    {
                        definition.type = this.config.typeEvaluator.eval( definition.type )  ;
                    }
                }
                if( definition.type instanceof Function )
                {
                    if ( definition.strategy )
                    {
                        instance = this.createObjectWithStrategy( definition.strategy ) ;
                    }
                    else
                    {
                        try
                        {
                            instance = invoke( definition.type , this.createArguments( definition.constructorArguments , definition.id ) ) ;
                        }
                        catch( e )
                        {
                            throw new Error( "can't create the instance with the specified definition type " + definition.type + ". The arguments limit exceeded, you can pass a maximum of 32 arguments" ) ;
                        }
                    }
                    if ( instance )
                    {
                        if( !definition.lazyType )
                        {
                            let check = false;
                            if( instance instanceof definition.type )
                            {
                                check = true ;
                            }
                            else if( definition.type === String )
                            {
                                check = (instance instanceof String) || (typeof(instance) === 'string') ;
                            }
                            else if( definition.type === Number )
                            {
                                check = (instance instanceof Number) || (typeof(instance) === 'number') ;
                            }
                            else if( definition.type === Boolean )
                            {
                                check = (instance instanceof Boolean) || (typeof(instance) === 'boolean') ;
                            }
                            if( !check )
                            {
                                instance = null ;
                                throw new Error( "the new object is not an instance of the [" + definition.type.name + "] constructor" ) ;
                            }
                        }
                        if ( definition.singleton )
                        {
                            this._singletons.set( id , instance ) ;
                        }
                        this.dependsOn( definition ) ;
                        this.populateIdentifiable ( instance , definition ) ;
                        let flag = isLockable( instance ) && ( ( definition.lock === true ) || ( this.config.lock === true && definition.lock !== false ) );
                        if ( flag )
                        {
                            instance.lock() ;
                        }
                        if( (definition.beforeListeners instanceof Array) && (definition.beforeListeners.length > 0) )
                        {
                            this.registerListeners( instance , definition.beforeListeners ) ;
                        }
                        if( (definition.beforeReceivers instanceof Array) && (definition.beforeReceivers.length > 0) )
                        {
                            this.registerReceivers( instance , definition.beforeReceivers ) ;
                        }
                        this.populateProperties( instance , definition ) ;
                        if( (definition.afterListeners instanceof Array) && (definition.afterListeners.length > 0) )
                        {
                            this.registerListeners( instance , definition.afterListeners ) ;
                        }
                        if( (definition.afterReceivers instanceof Array) && (definition.afterReceivers.length > 0) )
                        {
                            this.registerReceivers( instance , definition.afterReceivers ) ;
                        }
                        if ( flag )
                        {
                            instance.unlock() ;
                        }
                        this.invokeInitMethod( instance , definition ) ;
                        this.generates( definition ) ;
                    }
                }
                else
                {
                    throw new Error( "the definition.type property is not a valid constructor" ) ;
                }
            }
        }
        catch( er )
        {
            this.warn( this + " getObject('" + id + "') failed, " + er.message + "." ) ;
        }
        return instance || null ;
    }},
    isDirty : { value : function()
    {
        return this.bufferSingletons && (this.bufferSingletons instanceof Array) && this.bufferSingletons.length > 0 ;
    }},
    isLazyInit : { value : function( id )
    {
        if ( this.hasObjectDefinition( id ) )
        {
            return this.getObjectDefinition(id).lazyInit ;
        }
        else
        {
            return false ;
        }
    }},
    isSingleton : { value : function( id )
    {
        if ( this.hasObjectDefinition( id ) )
        {
            return this.getObjectDefinition(id).singleton ;
        }
        else
        {
            return false ;
        }
    }},
    removeSingleton : { value : function( id )
    {
        if ( this.isSingleton(id) && this._singletons.has(id) )
        {
            this.invokeDestroyMethod( id ) ;
            this._singletons.delete( id ) ;
        }
    }},
    run : { value : function( ...args )
    {
        if ( this.running )
        {
            return ;
        }
        this.notifyStarted() ;
        if ( args.length > 0 && (args[0] instanceof Array) )
        {
            this.objects = args[0] ;
        }
        if ( this.bufferSingletons === null )
        {
            this.bufferSingletons = [] ;
        }
        if ( (this.objects instanceof Array) && this.objects.length > 0)
        {
            while ( this.objects.length > 0 )
            {
                let init = this.objects.shift();
                if ( init !== null )
                {
                    let definition = createObjectDefinition( init );
                    this.addObjectDefinition( definition ) ;
                    if ( definition.singleton && !definition.lazyInit )
                    {
                        if ( this.hasObjectDefinition( definition.id ) )
                        {
                            this.bufferSingletons.push( String( definition.id ) ) ;
                        }
                    }
                }
                else
                {
                    this.warn( this + " create new object definition failed with a 'null' or 'undefined' object." ) ;
                }
            }
        }
        if ( (this.bufferSingletons instanceof Array) && this.bufferSingletons.length > 0 && !this._config.lazyInit && !this.isLocked() )
        {
            let len = this.bufferSingletons.length;
            for ( let i = 0 ; i < len ; i++ )
            {
                this.getObject( this.bufferSingletons[i] ) ;
            }
            this.bufferSingletons = null ;
        }
        this.notifyFinished() ;
    }},
    warn : { value : function( ...args )
    {
        if ( this.config.useLogger )
        {
            logger.warning.apply( logger , args ) ;
        }
    }},
    createArguments : { value : function( args = null , id = null )
    {
        if ( args === null || !(args instanceof Array) || args.length === 0 )
        {
            return null ;
        }
        let stack = [];
        let len = args.length;
        for ( let i = 0 ; i < len ; i++ )
        {
            let item = args[i];
            if( item instanceof ObjectArgument )
            {
                let value = item.value;
                try
                {
                    let alert = null;
                    if ( item.policy === ObjectAttribute.CALLBACK )
                    {
                        let callback = value;
                        if( value instanceof String || typeof(value) === 'string' )
                        {
                            callback = this._config.referenceEvaluator.eval( value ) ;
                        }
                        if( callback instanceof Function )
                        {
                            if( item.scope )
                            {
                                if( item.args instanceof Array )
                                {
                                    callback = value.bind.apply( item.scope , [item.scope].concat( this.createArguments( item.args , id ) ) ) ;
                                }
                                else
                                {
                                    callback = value.bind( item.scope ) ;
                                }
                            }
                            value = callback  ;
                        }
                        else
                        {
                            alert = ObjectAttribute.CALLBACK ;
                            value = null ;
                        }
                    }
                    if ( item.policy === ObjectAttribute.REFERENCE )
                    {
                        value = this._config.referenceEvaluator.eval( value ) ;
                        if( value === null )
                        {
                            alert = ObjectAttribute.FACTORY ;
                        }
                    }
                    else if ( item.policy === ObjectAttribute.CONFIG )
                    {
                        value = this._config.configEvaluator.eval( value ) ;
                        if( value === null )
                        {
                            alert = ObjectAttribute.LOCALE ;
                        }
                    }
                    else if ( item.policy === ObjectAttribute.LOCALE )
                    {
                        value = this._config.localeEvaluator.eval( value ) ;
                        if( value === null )
                        {
                            alert = ObjectAttribute.LOCALE ;
                        }
                    }
                    if( alert !== null )
                    {
                        this.warn( this + " createArguments failed at the index '" + i + "' and return a 'null' " + alert + " reference, see the object definition with the id : " + id ) ;
                    }
                    if ( item.evaluators !== null && item.evaluators.length > 0 )
                    {
                        value = this.eval( value , item.evaluators  ) ;
                    }
                    stack.push( value ) ;
                }
                catch( er )
                {
                    this.warn( this + " createArguments failed in the object definition with the id : " + id + ", " + er.toString() ) ;
                }
            }
        }
        return stack ;
    }},
    createObjectWithStrategy : { value : function( strategy , id = null )
    {
        if ( !(strategy instanceof ObjectStrategy) )
        {
            return null ;
        }
        let name = strategy.name;
        let instance = null;
        let object;
        let ref;
        if ( strategy instanceof ObjectMethod )
        {
            if ( strategy instanceof ObjectStaticFactoryMethod )
            {
                object = strategy.type ;
                if( object instanceof String || typeof(object) === 'string' )
                {
                    object = this.config.typeEvaluator.eval( object ) ;
                }
                if ( object && name && (name in object) && (object[name] instanceof Function) )
                {
                    instance = object[name].apply( object , this.createArguments( strategy.args , id ) ) ;
                }
            }
            else if ( strategy instanceof ObjectFactoryMethod )
            {
                ref = this.getObject( strategy.factory ) ;
                if ( ref && name && (name in ref) && (ref[name] instanceof Function) )
                {
                    instance = ref[name].apply( ref , this.createArguments( strategy.args , id ) ) ;
                }
            }
        }
        else if ( strategy instanceof ObjectProperty )
        {
            if ( strategy instanceof ObjectStaticFactoryProperty )
            {
                object = strategy.type ;
                if( object instanceof String || typeof(object) === 'string' )
                {
                    object = this.config.typeEvaluator.eval( object ) ;
                }
                if ( object && name && (name in object) )
                {
                    instance = object[name] ;
                }
            }
            else if ( strategy instanceof ObjectFactoryProperty )
            {
                ref = this.getObject( strategy.factory ) ;
                if ( ref && name && (name in ref) )
                {
                    instance = ref[name] ;
                }
            }
        }
        else if ( strategy instanceof ObjectValue )
        {
            instance = strategy.value ;
        }
        else if ( strategy instanceof ObjectReference )
        {
            instance = this._config.referenceEvaluator.eval( strategy.ref ) ;
        }
        return instance ;
    }},
    dependsOn : { value : function( definition )
    {
        if ( (definition instanceof ObjectDefinition) && (definition.dependsOn instanceof Array) && (definition.dependsOn.length > 0 ) )
        {
            let id;
            let len = definition.dependsOn.length;
            for ( let i = 0 ; i<len ; i++ )
            {
                id = definition.dependsOn[i] ;
                if ( this.hasObjectDefinition(id) )
                {
                    this.getObject(id) ;
                }
            }
        }
    }} ,
    eval : { value : function( value , evaluators = null )
    {
        if ( !(evaluators instanceof Array) || (evaluators.length === 0) )
        {
            return value ;
        }
        this._evaluator.clear() ;
        let o;
        let s = evaluators.length;
        let a = [];
        for ( let i = 0 ; i < s ; i++ )
        {
            o = evaluators[i] ;
            if ( o === null )
            {
                continue ;
            }
            if ( o instanceof String || typeof(o) === 'string' )
            {
                o = this.getObject( o ) ;
            }
            if ( o instanceof Evaluable )
            {
                a.push( o ) ;
            }
        }
        if ( a.length > 0 )
        {
            this._evaluator.add( a ) ;
            value = this._evaluator.eval( value ) ;
            this._evaluator.clear() ;
        }
        return value ;
    }},
    generates : { value : function( definition )
    {
        if ( (definition instanceof ObjectDefinition) && ( definition.generates instanceof Array ) )
        {
            let ar = definition.generates;
            let len = ar.length;
            if ( len > 0 )
            {
                for ( let i = 0 ; i<len ; i++ )
                {
                   let id = ar[i];
                   if ( this.hasObjectDefinition(id) )
                   {
                       this.getObject(id) ;
                   }
                }
            }
        }
    }},
    invokeDestroyMethod : { value : function( id )
    {
        if( this.hasObjectDefinition(id) && this._singletons.has(id) )
        {
            let definition = this.getObjectDefinition(id);
            let o = this._singletons.get(id);
            let name = definition.destroyMethodName || null;
            if ( name === null && this.config !== null )
            {
                name = this.config.defaultDestroyMethod ;
            }
            if( name && (name in o) && (o[name] instanceof Function) )
            {
                o[name].call(o) ;
            }
        }
    }},
    invokeInitMethod : { value : function( o , definition = null )
    {
        if( definition && (definition instanceof ObjectDefinition) )
        {
            let name = definition.initMethodName || null;
            if ( (name === null) && this.config )
            {
                name = this.config.defaultInitMethod || null ;
            }
            if( name && (name in o) && (o[name] instanceof Function) )
            {
                o[name].call(o) ;
            }
        }
    }},
    populateIdentifiable : { value : function( o , definition = null )
    {
        if( definition && (definition instanceof ObjectDefinition) )
        {
            if ( definition.singleton && isIdentifiable(o) )
            {
                if ( ( definition.identify === true ) || ( this.config.identify === true && definition.identify !== false ) )
                {
                    o.id = definition.id ;
                }
            }
        }
    }},
    populateProperties : { value : function( o , definition                      = null )
    {
        if( definition && (definition instanceof ObjectDefinition) )
        {
            let properties = definition.properties;
            if ( properties && (properties instanceof Array) && properties.length > 0 )
            {
                let id  = definition.id;
                let len = properties.length;
                for( let i = 0 ; i < len ; i++ )
                {
                    this.populateProperty( o , properties[i] , id ) ;
                }
            }
        }
    }},
    populateProperty : { value : function( o , prop , id )
    {
        if ( o === null )
        {
            this.warn( this + " populate a new property failed, the object not must be 'null' or 'undefined', see the factory with the object definition '" + id + "'." ) ;
            return ;
        }
        let name  = prop.name;
        let value = prop.value;
        if( name === MagicReference.INIT )
        {
            if ( (prop.policy === ObjectAttribute.REFERENCE) && (value instanceof String || typeof(value) === 'string' ))
            {
                value = this._config.referenceEvaluator.eval( value ) ;
            }
            else if ( prop.policy === ObjectAttribute.CONFIG )
            {
                value = this.config.configEvaluator.eval( value ) ;
            }
            else if ( prop.policy === ObjectAttribute.LOCALE )
            {
                value = this.config.localeEvaluator.eval( value) ;
            }
            if ( prop.evaluators && prop.evaluators.length > 0 )
            {
                value = this.eval( value , prop.evaluators ) ;
            }
            if ( value )
            {
                for( var member in value )
                {
                    if( member in o )
                    {
                        o[member] = value[member] ;
                    }
                    else
                    {
                        this.warn( this + " populateProperty failed with the magic #init name, the " + member + " attribute is not declared on the object with the object definition '" + id + "'." ) ;
                    }
                }
            }
            else
            {
                this.warn( this + " populate a new property failed with the magic #init name, the object to enumerate not must be null, see the factory with the object definition '" + id + "'." ) ;
            }
            return ;
        }
        if ( !( name in o ) )
        {
            this.warn( this + " populate a new property failed with the " + name + " attribute, this property is not registered in the object, see the object definition '" + id + "'." ) ;
            return ;
        }
        try
        {
            if ( prop.policy === ObjectAttribute.CALLBACK )
            {
                if( value instanceof String || typeof(value) === 'string' )
                {
                    value = this._config.referenceEvaluator.eval( value ) ;
                    if( value === null )
                    {
                        this.warn( this + " populateProperty with the name '" + name + "' return a null callback reference, see the object definition with the id : " + id ) ;
                    }
                }
                if( value instanceof Function )
                {
                    if( prop.scope )
                    {
                        if( prop.args instanceof Array )
                        {
                            value = value.bind.apply( prop.scope , [prop.scope].concat( this.createArguments( prop.args , id ) ) ) ;
                        }
                        else
                        {
                            value = value.bind( prop.scope ) ;
                        }
                    }
                    value = value  ;
                }
                else
                {
                    value = null ;
                }
            }
            else if( prop.policy === ObjectAttribute.REFERENCE )
            {
                value = this._config.referenceEvaluator.eval( value ) ;
                if( value === null )
                {
                    this.warn( this + " populateProperty with the name '" + name + "' return a 'null' factory reference, see the object definition with the id : " + id ) ;
                }
            }
            else if( prop.policy === ObjectAttribute.CONFIG )
            {
                value = this.config.configEvaluator.eval( value ) ;
                if( value === null )
                {
                    this.warn( this + " populateProperty with the name '" + name + "' return a 'null' config reference, see the object definition with the id : " + id ) ;
                }
            }
            else if( prop.policy === ObjectAttribute.LOCALE )
            {
                value = this.config.localeEvaluator.eval( value ) ;
                if( value === null )
                {
                    this.warn( this + " populateProperty with the name '" + name + "' return a null locale reference, see the object definition with the id : " + id ) ;
                }
            }
            else if( o[name] instanceof Function )
            {
                if( prop.policy === ObjectAttribute.ARGUMENTS )
                {
                    o[name].apply( o , this.createArguments( value , id ) ) ;
                    return ;
                }
                else
                {
                    o[name]() ;
                    return ;
                }
            }
            if ( prop.evaluators && prop.evaluators.length > 0 )
            {
                value = this.eval( value , prop.evaluators ) ;
            }
            o[ name ] = value ;
        }
        catch( e )
        {
            this.warn( this + " populateProperty failed with the name '" + name + ", see the object definition '" + id + "', error: " + e.toString() ) ;
        }
    }},
    registerListeners : { value : function( o , listeners )
    {
        if ( o === null || listeners === null || !(listeners instanceof Array) )
        {
            return ;
        }
        let len = listeners.length;
        if ( len > 0 )
        {
            for ( let i = 0 ; i<len ; i++ )
            {
                try
                {
                    let entry = listeners[i];
                    let dispatcher = this._config.referenceEvaluator.eval( entry.dispatcher );
                    if( dispatcher && entry.type !== null )
                    {
                        let listener;
                        if ( dispatcher instanceof IEventDispatcher )
                        {
                            if ( entry.method && (entry.method in o) && (o[entry.method] instanceof Function))
                            {
                                listener = o[entry.method].bind(o) ;
                            }
                            else if( o instanceof EventListener )
                            {
                                listener = o ;
                            }
                            dispatcher.addEventListener( entry.type , listener , entry.useCapture , entry.priority ) ;
                        }
                        else if ( ("addEventListener" in dispatcher) && (dispatcher.addEventListener instanceof Function) )
                        {
                            if ( entry.method && (entry.method in o) && (o[entry.method] instanceof Function))
                            {
                                listener = o[entry.method].bind(o) ;
                            }
                            else if( o instanceof EventListener )
                            {
                                listener = o.handleEvent.bind(o) ;
                            }
                            dispatcher.addEventListener( entry.type , listener , entry.useCapture ) ;
                        }
                    }
                }
                catch( e )
                {
                    this.warn( this + " registerListeners failed with the target '" + o + "' , in the collection of this listeners at {" + i + "} : " + e.toString() ) ;
                }
            }
        }
    }},
    registerReceivers : { value : function( o , receivers  = null )
    {
        if ( !(receivers instanceof Array) || (receivers.length === 0) )
        {
            return ;
        }
        let len = receivers.length;
        for( let i = 0 ; i<len ; i++ )
        {
            try
            {
                let receiver = receivers[i];
                let signaler = this._config.referenceEvaluator.eval( receiver.signal );
                let slot     = null;
                if ( signaler instanceof Signaler )
                {
                    if ( (receiver.slot instanceof String || typeof(receiver.slot) === 'string') && (receiver.slot in o) && ( o[receiver.slot] instanceof Function ) )
                    {
                        slot = o[receiver.slot] ;
                    }
                    else if ( o instanceof Receiver )
                    {
                        slot = o ;
                    }
                    if ( (slot instanceof Receiver) || (slot instanceof Function) )
                    {
                        signaler.connect( slot , receiver.priority, receiver.autoDisconnect ) ;
                    }
                }
            }
            catch( e )
            {
                this.warn( this + " registerReceivers failed with the target '" + o + "' , in the collection of this receivers at {" + i + "} : " + e.toString() ) ;
            }
        }
    }}
}) ;

var config = {
    'origin': { x: 10, y: 20 },
    'nirvana': { x: 0, y: 0 }
};

var i18n = {
    messages: {
        test: 'test'
    }
};

var factory = new ObjectFactory();
factory.config.setConfigTarget(config);
factory.config.setLocaleTarget(i18n);

function LineFormattedTarget( init = null )
{
    LoggerTarget.call( this ) ;
    Object.defineProperties( this ,
    {
        _lineNumber : { value : 1 , writable : true }
    }) ;
    this.includeChannel = false ;
    this.includeDate = false ;
    this.includeLevel = false ;
    this.includeLines = false ;
    this.includeMilliseconds = false ;
    this.includeTime = false ;
    this.separator = " " ;
    if( init )
    {
        for( var prop in init )
        {
            if ( this.hasOwnProperty(prop) )
            {
                this[prop] = init[prop] ;
            }
        }
    }
}
LineFormattedTarget.prototype = Object.create( LoggerTarget.prototype ,
{
    constructor : { value : LineFormattedTarget , writable : true } ,
    internalLog : { value : function( message , level )
    {
    }},
    toString : { writable : true , value : function() { return '[' + this.constructor.name + ']' ; } } ,
    logEntry : { value : function( entry )
    {
        var message = this.formatMessage
        (
            entry.message,
            LoggerLevel.getLevelString( entry.level ),
            entry.channel ,
            new Date()
        );
        this.internalLog( message , entry.level ) ;
    }},
    resetLineNumber : { value : function()
    {
        this._lineNumber = 1 ;
    }},
    formatDate : { value : function( d )
    {
        var date  = "";
        date += this.getDigit( d.getDate() ) ;
        date += "/" + this.getDigit( d.getMonth() + 1 ) ;
        date += "/" + d.getFullYear() ;
        return date ;
    }},
    formatLevel : { value : function( level  )
    {
        return '[' + level + ']' ;
    }},
    formatLines : { value : function()
    {
        return "[" + this._lineNumber++ + "]" ;
    }},
    formatMessage : { value : function( message , level  , channel  , date          )
    {
        var msg = "";
        if (this.includeLines)
        {
            msg += this.formatLines() + this.separator ;
        }
        if( this.includeDate || this.includeTime )
        {
            date = date || new Date() ;
            if (this.includeDate)
            {
                msg += this.formatDate(date) + this.separator ;
            }
            if (this.includeTime)
            {
                msg += this.formatTime(date) + this.separator ;
            }
        }
        if (this.includeLevel)
        {
            msg += this.formatLevel(level || "" ) + this.separator ;
        }
        if ( this.includeChannel )
        {
            msg += ( channel || "" ) + this.separator ;
        }
        msg += message ;
        return msg ;
    }},
    formatTime : { value : function( d )
    {
        var time  = "";
        time += this.getDigit(d.getHours()) ;
        time += ":" + this.getDigit(d.getMinutes()) ;
        time += ":" + this.getDigit(d.getSeconds()) ;
        if( this.includeMilliseconds )
        {
            time += ":" + this.getDigit( d.getMilliseconds() ) ;
        }
        return time ;
    }},
    getDigit : { value : function( n )
    {
        if ( isNaN(n) )
        {
            return "00" ;
        }
        return ((n < 10) ? "0" : "") + n ;
    }}
});

function ConsoleTarget( init = null )
{
    LineFormattedTarget.call( this , init ) ;
}
ConsoleTarget.prototype = Object.create( LineFormattedTarget.prototype ,
{
    constructor : { value : ConsoleTarget } ,
    internalLog : { value : function( message , level )
    {
        if( console )
        {
            switch( level )
            {
                case LoggerLevel.CRITICAL :
                {
                    console.trace( message ) ;
                    break ;
                }
                case LoggerLevel.DEBUG :
                {
                    if( console.debug )
                    {
                        console.debug( message ) ;
                    }
                    else if ( console.trace )
                    {
                        console.trace( message ) ;
                    }
                    break ;
                }
                case LoggerLevel.ERROR :
                {
                    console.error( message ) ;
                    break ;
                }
                case LoggerLevel.INFO :
                {
                    console.info( message ) ;
                    break ;
                }
                case LoggerLevel.WARNING :
                {
                    console.warn( message ) ;
                    break ;
                }
                default :
                case LoggerLevel.ALL :
                {
                    console.log( message ) ;
                    break ;
                }
            }
        }
        else
        {
            throw new new ReferenceError('The console reference is unsupported.') ;
        }
    }}
});

var logger$1 = Log.getLogger('channel');
var target = new ConsoleTarget({
    includeChannel: true,
    includeDate: false,
    includeLevel: true,
    includeLines: true,
    includeMilliseconds: true,
    includeTime: true
});
target.filters = ['*'];
target.level = LoggerLevel.ALL;

var Point = function Point() {
    var x = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : 0;
    var y = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : 0;
    this.x = x;
    this.y = y;
};
Point.prototype.destroy = function () {
    logger$1.info(this + " destroy");
};
Point.prototype.init = function () {
    logger$1.info(this + " init");
};
Point.prototype.test = function () {
    var message = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : '';
    logger$1.info(this + ' test message: ' + message);
};
Point.prototype.toString = function () {
    return "[Point x:" + this.x + " y:" + this.y + "]";
};

var Slot = function Slot() {
    logger$1.debug(this + ' constructor invoked');
};
Slot.prototype = Object.create(Receiver.prototype, {
    constructor: { value: Slot },
    receive: { value: function value(message) {
            logger$1.info('slot receive ' + (message || 'an unknow message...'));
        } },
    toString: { value: function value() {
            return '[Slot]';
        } }
});

var definitions = [{
    id: "signal",
    type: Signal,
    dependsOn: ['slot'],
    singleton: true,
    lazyInit: true
}, {
    id: "slot",
    type: Slot,
    singleton: true,
    lazyInit: true,
    receivers: [{ signal: "signal" }]
}, {
    id: "position",
    type: Point,
    args: [{ value: 2 }, { ref: 'origin.y' }],
    sigleton: true,
    lazyInit: true,
    init: 'init',
    properties: [{ name: "x", ref: 'origin.x' }, { name: "y", value: 100 }, { name: '#init', config: 'nirvana' }]
}, {
    id: "origin",
    type: Point,
    singleton: true,
    lazyInit: true,
    args: [{ config: 'origin.x' }, { value: 20 }],
    properties: [{ name: 'test', args: [{ locale: 'messages.test' }] }]
}];

function main() {
    logger$1.info('VEGAS JS SKELETON ----- START');
    factory.run(definitions);
    var position = factory.getObject('position');
    logger$1.info(position);
    var signal = factory.getObject('signal');
    if (signal) {
        signal.emit('hello world');
    } else {
        logger$1.warning('The slot reference not must be null or undefined.');
    }
}

exports.main = main;

Object.defineProperty(exports, '__esModule', { value: true });

})));
