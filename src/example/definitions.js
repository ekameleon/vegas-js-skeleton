"use strict" ;

// --------- Dependencies

import { Signal } from 'system/signals/Signal.js' ;

import { Point } from './geom/Point.js' ;
import { Slot  } from './receivers/Slot.js' ;

// ---------  Object Definitions

export var definitions =
[
    {
        id        : "signal" ,
        type      : Signal ,
        dependsOn : [ 'slot' ] ,
        singleton : true ,
        lazyInit  : true
    },
    {
        id        : "slot" ,
        type      : Slot ,
        singleton : true ,
        lazyInit  : true ,
        receivers : [ { signal : "signal" } ]
    },
    {
        id   : "position" ,
        type : Point ,
        args : [ { value : 2 } , { ref : 'origin.y' }],
        sigleton   : true ,
        lazyInit   : true ,
        init       : 'init' ,
        properties :
        [
            { name : "x" , ref   :'origin.x' } ,
            { name : "y" , value : 100       } ,
            { name : '#init' , config : 'nirvana' }
        ]
    },
    {
        id         : "origin" ,
        type       : Point ,
        singleton  : true ,
        lazyInit   : true ,
        args       : [ { config : 'origin.x' } , { value : 20 }] ,
        properties :
        [
            { name : 'test' , args : [ { locale : 'messages.test' } ] }
        ]
    }
];