"use strict" ;

import { logger } from '../logging/logger.js' ;

export var Point = function( x = 0 , y = 0 )
{
    this.x = x ;
    this.y = y ;
};

Point.prototype.destroy = function()
{
    logger.info( this + " destroy" ) ;
}

Point.prototype.init = function()
{
    logger.info( this + " init" ) ;
}

Point.prototype.test = function( message = '' )
{
    logger.info( this + ' test message: ' + message ) ;
}

Point.prototype.toString = function()
{
    return "[Point x:" + this.x + " y:" + this.y + "]" ;
} ;