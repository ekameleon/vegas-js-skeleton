"use strict" ;

import { Log }           from 'system/logging/Log.js' ;
import { ConsoleTarget } from 'system/logging/targets/ConsoleTarget.js' ;
import { LoggerLevel }   from 'system/logging/LoggerLevel.js' ;

export var logger = Log.getLogger('channel') ;

var target = new ConsoleTarget
({
    includeChannel      : true  ,
    includeDate         : false ,
    includeLevel        : true  ,
    includeLines        : true  ,
    includeMilliseconds : true  ,
    includeTime         : true
}) ;

target.filters = ['*'] ;
target.level   = LoggerLevel.ALL ;