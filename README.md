# VEGAS JS SKELETON #

The Vegas JS Skeleton example (version 1.0.7) based on the VEGAS JS (version 1.0.9).

Use this skeleton application to quickly setup and start working on a the new VEGAS JS library. This application project use NPM to download all the dependencies of the latest VEGAS JS  version.

### Licenses ###

Under tree opensource licenses : **MPL 1.1/GPL 2.0/LGPL 2.1**

 * [License MPL 2.0](https://www.mozilla.org/en-US/MPL/2.0/)
 * [License GPL 2.0+](https://www.gnu.org/licenses/gpl-2.0.html)
 * [License LGPL 2.1+](https://www.gnu.org/licenses/lgpl-2.1.html)

### Install ###

#### ⌜ YARN / NPM

You can install VEGAS JS with [NPM](https://www.npmjs.com/package/vegas-js) or [Yarn](https://yarnpkg.com/).

```
$ yarn add vegas-js-skeleton --dev
```

```
#!shell
npm install vegas-js-skeleton --save-dev
```

### Usage ###

#### Init the project ####

```
$ yarn
```

#### Compilation

See first the **config.json** and **rollup.config.js** files on the main project directory.

1 - Generates the **./dist/js/application.js** library
```
$ yarn dev
```

2 - Generates the **./dist/js/application.min.js** library
```
$ yarn prod
```

3 - Use the --watch option to update your application library when your code change.
```
$ yarn dev --watch
```
**Note** : Press **CTRL** + **C** to exit the watching process.

4 - Build the **./dist/js/application.js** and the **./dist/js/application.min.js** library
```
$ yarn build
```

#### Use your application library in your HTML page ####

See the source code of the **./dist/index.html** page :
```
#!html
<!doctype html>
<html>
    <head>
        <title>Application - VEGAS JS Skeleton</title>
        <meta name="description" content="Application example with Vegas JS">
        <meta charset="UTF-8">
        <script src="./js/application.min.js"></script>
    </head>

    <body>
        <script>
        window.onload = application.main ; // run the application
        </script>
    </body>
</html>
```

### About ###

 * Author : Marc ALCARAZ (aka eKameleon) - Creative Technologist and Digital Architect
 * Mail : ekameleon[at]gmail.com
 * LinkedIn : [https://www.linkedin.com/in/ekameleon/](https://www.linkedin.com/in/ekameleon/)

### History ###

 * 1998 : Flash
 * 2000 : First framework concept and first libraries (components, tools, design patterns)
 * 2004 : First official SVN repository
 * 2007 : Fusion with the Maashaack framework (eden, etc.)
 * 2015 : Google Code must die - **VEGAS** move from an old Google Code SVN repository to this Bitbucket GIT repository and REBOOT this source code.
 * 2016 : Begin the new JS architecture of the VEGAS JS library based on ES6