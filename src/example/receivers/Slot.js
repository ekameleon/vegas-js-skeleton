"use strict" ;

import { Receiver } from 'system/signals/Receiver.js' ;
import { logger   } from '../logging/logger.js' ;

export var Slot = function()
{
    logger.debug( this + ' constructor invoked') ;
}

Slot.prototype = Object.create( Receiver.prototype ,
{
    constructor : { value : Slot } ,
    receive     : { value : function( message )
    {
        logger.info( 'slot receive ' + (message || 'an unknow message...') ) ;
    }},
    toString : { value : function() { return '[Slot]' ; } }
});