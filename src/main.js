"use strict" ;

//////////////////////////////// IMPORTS

import { factory }     from './example/factory.js' ;
import { logger }      from './example/logging/logger.js' ;
import { definitions } from './example/definitions.js' ;

//////////////////////////////// MAIN

export function main()
{
    //////////////////////////////// INITIALIZE

    logger.info('VEGAS JS SKELETON ----- START');

    factory.run( definitions );

    //////////////////////////////// EXAMPLES

    var position = factory.getObject('position') ;

    logger.info( position ) ;

    var signal = factory.getObject('signal') ;
    if( signal )
    {
        signal.emit( 'hello world' ) ;
    }
    else
    {
        logger.warning( 'The slot reference not must be null or undefined.' );
    }

    //////////////////////////////// END
}

//////////////////////////////// THANK YOU

