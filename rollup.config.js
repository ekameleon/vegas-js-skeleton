"use strict" ;

import alias from 'rollup-plugin-alias';
import babel from 'rollup-plugin-babel';
import cleanup from 'rollup-plugin-cleanup';
import license from 'rollup-plugin-license' ;
import path from 'path' ;
import replace from 'rollup-plugin-replace';
import minify from 'rollup-plugin-babel-minify';

import pkg from './package.json' ;

let dev = !process.env.prod ;
let vegas = './node_modules/vegas-js/src/' ;

let setting ;

try
{
    setting = require('./user.json') ;
}
catch (e)
{
    setting = require('./config.json') ;
}

let header =
`/**
 * ${pkg.description} - version: ${pkg.version} - license: MPL 2.0/GPL 2.0+/LGPL 2.1+ - Follow me on Twitter! @ekameleon
 */` ;

let libraries =
{
    'core'     : path.resolve( __dirname, vegas + 'core/' ) ,
    'graphics' : path.resolve( __dirname, vegas + 'graphics/' ) ,
    'molecule' : path.resolve( __dirname, vegas + 'molecule/' ) ,
    'polyfill' : path.resolve( __dirname, vegas + 'polyfill/' ) ,
    'screens'  : path.resolve( __dirname, vegas + 'screens/' ) ,
    'system'   : path.resolve( __dirname, vegas + 'system/' ) ,
    '@vegas'   : path.resolve( __dirname, vegas + '' )
};

let metas =
{
    NAME        : pkg.name ,
    DESCRIPTION : pkg.description ,
    HOMEPAGE    : pkg.homepage ,
    LICENSE     : pkg.license ,
    VERSION     : pkg.version
};

let plugins =
[
    alias( libraries ),
    babel
    ({
        exclude : 'node_modules/**' ,
        babelrc : false ,
        presets : [ [ "env" , { "modules" : false } ] ]
    }),
    replace
    ({
        delimiters : [ '<@' , '@>' ] ,
        values     : metas
    }),
    cleanup()
] ;

if( !dev )
{
    plugins.push( minify
    ({
        banner    : header ,
        sourceMap : setting.sourcemap
    }));
}
else
{
    plugins.push( license( { banner:header } ) ) ;
}

export default
{
    input  : setting.entry ,
    output :
    {
        name   : setting.bundle ,
        file   : setting.output + setting.file + (dev ? '' : '.min') + '.js' ,
        format : 'umd' ,
        strict : true
    },
    plugins : plugins ,
    watch   : { exclude : 'node_modules/**' }
};